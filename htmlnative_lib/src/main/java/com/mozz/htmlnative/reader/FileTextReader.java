package com.mozz.htmlnative.reader;

import java.io.*;

/**
 * Created by Yang Tao on 17/2/21.
 */

public class FileTextReader extends StreamReader {

//    public FileTextReader( File file) throws FileNotFoundException {
//        super(new FileReader(file));
//    }

    public FileTextReader( InputStream stream) {
        super(new InputStreamReader(stream));
    }
}
