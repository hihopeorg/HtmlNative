package com.mozz.htmlnative;

import com.mozz.htmlnative.css.stylehandler.StyleHandler;
import com.mozz.htmlnative.dom.DomElement;
import com.mozz.htmlnative.exception.AttrApplyException;
import com.mozz.htmlnative.view.LayoutParamsCreator;
import ohos.agp.components.Component;
import ohos.app.Context;

import java.util.Set;

/**
 * @author Yang Tao, 17/3/3.
 */

public abstract class HNViewType<T extends Component> extends StyleHandler implements HNRenderer
        .ViewFactory<T> {

    
    public abstract Class<T> getViewClass();

    
    public abstract String getHTMLType();

    @Override
    public final void apply(Context context, Component v, DomElement domElement, Component parent,
                            LayoutParamsCreator paramsCreator, String params, Object
                                        value) throws AttrApplyException {
        this.onSetStyle(context, v, parent, paramsCreator, params, value);
    }

    @Override
    public final void setDefault(Context context, Component v, DomElement domElement,
                                 LayoutParamsCreator paramsCreator, Component parent) throws
            AttrApplyException {
        this.onSetDefaultStyle(context, v, paramsCreator, parent);
    }

    public abstract void onSetStyle(Context context, Component v, Component parent, LayoutParamsCreator
            layoutCreator, String styleName, Object style);

    public abstract void onSetDefaultStyle(Context context, Component v, LayoutParamsCreator layoutParamsCreator, Component parent);

    public Set<String> onInheritStyleNames() {
        return null;
    }
}
