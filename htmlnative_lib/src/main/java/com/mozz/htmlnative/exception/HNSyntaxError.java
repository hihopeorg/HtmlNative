package com.mozz.htmlnative.exception;

public class HNSyntaxError extends Exception {

    public HNSyntaxError(String msg) {
        super(msg);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
