package com.mozz.htmlnative.view;

import com.mozz.htmlnative.css.Background;
import ohos.agp.components.element.Element;
import ohos.media.image.PixelMap;

/**
 * @author Yang Tao, 17/5/9.
 */
interface IBackgroundManager {
    void setHtmlBackground(PixelMap bitmap, Background background);

    void setHtmlBackground(Element drawable, Background background);

    Background getHtmlBackground();
}
