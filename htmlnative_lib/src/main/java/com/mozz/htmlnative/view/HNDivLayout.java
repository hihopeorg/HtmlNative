package com.mozz.htmlnative.view;

import com.mozz.htmlnative.IntDef;
import com.mozz.htmlnative.css.Background;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.element.Element;
import ohos.agp.render.Canvas;
import ohos.app.Context;
import ohos.media.image.PixelMap;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.*;

/**
 * @author Yang Tao, 17/4/18.
 */

public class HNDivLayout extends ComponentContainer implements IBackgroundView, Component.EstimateSizeListener, ComponentContainer.ArrangeListener, Component.DrawTask {

    private List<Integer> mLineIndexes = new ArrayList<>();
    private List<Component> mFloatViews = new LinkedList<>();
    private List<Integer> mLineHeights = new ArrayList<>();
    private BackgroundManager mBackgroundMgr;
    private Map<String, Object> mSavedInheritStyles = new HashMap<>();

    public HNDivLayout(Context context) {
        this(context, null);
        mBackgroundMgr = new BackgroundManager(this);
    }

    public HNDivLayout(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
        mBackgroundMgr = new BackgroundManager(this);
    }

    public HNDivLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        mBackgroundMgr = new BackgroundManager(this);
        init();
    }

    public void init() {
        setEstimateSizeListener(this);
        setArrangeListener(this);
        addDrawTask(this);
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        mFloatViews.clear();

        final int msWidth = EstimateSpec.getSize(widthMeasureSpec);
        final int msHeight = EstimateSpec.getSize(heightMeasureSpec);
        int widthMode = EstimateSpec.getMode(widthMeasureSpec);
        int heightMode = EstimateSpec.getMode(heightMeasureSpec);
        int width = 0;
        int height = getPaddingLeft();
        int length = getChildCount();
        int lineWidth = getPaddingLeft();
        int lineHeight = 0;
        boolean firstLine = true;
        for (int i = 0; i < length; i++) {
            Component child = getComponentAt(i);
            child.estimateSize(widthMeasureSpec, heightMeasureSpec);
            HNDivLayoutParams lp = (HNDivLayoutParams) child.getLayoutConfig();

            if (lp.positionMode == HNDivLayoutParams.POSITION_STATIC || lp.positionMode ==
                    HNDivLayoutParams.POSITION_RELATIVE) {

                int childWidth = child.getEstimatedWidth() + lp.getMarginLeft() + lp.getMarginRight();
                int childHeight = child.getEstimatedHeight() + lp.getMarginTop() + lp.getMarginBottom();

                if (childWidth + lineWidth + getPaddingRight() > msWidth) {
                    width = Math.max(lineWidth + getPaddingRight(), childWidth + getPaddingLeft());
                    height += lineHeight;

                    lineWidth = childWidth + getPaddingLeft();
                    lineHeight = childHeight;

                    if (firstLine) {
                        firstLine = false;
                    }
                } else {
                    lineWidth += childWidth;
                    lineHeight = Math.max(lineHeight, childHeight);
                }
            } else if (lp.positionMode == HNDivLayoutParams.POSITION_FLOAT_LEFT || lp
                    .positionMode == HNDivLayoutParams.POSITION_FLOAT_RIGHT) {
                mFloatViews.add(child);
            }
        }

        width = Math.max(lineWidth, width);
        height += lineHeight;

        height = height + getPaddingBottom();

        setEstimatedSize(widthMode == EstimateSpec.PRECISE ? msWidth : width, heightMode ==
                EstimateSpec.PRECISE ? msHeight : height);
        return false;
    }

    @Override
    public void addComponent(Component child, LayoutConfig params) {
        if (!(params instanceof HNDivLayoutParams)) {
            super.addComponent(child, new HNDivLayoutParams(params));
        } else {
            super.addComponent(child, params);
        }
    }

    @Override
    public boolean onArrange(int l, int t, int r, int b) {
        mLineHeights.clear();
        mLineIndexes.clear();

        int width = getWidth();

        int lineWidth = 0;
        int lineHeight = 0;

        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();

        final int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            Component child = getComponentAt(i);
            HNDivLayoutParams lp = (HNDivLayoutParams) child.getLayoutConfig();
            int childWidth = child.getEstimatedWidth();
            int childHeight = child.getEstimatedHeight();

            if (lp.positionMode == HNDivLayoutParams.POSITION_STATIC || lp.positionMode ==
                    HNDivLayoutParams.POSITION_RELATIVE) {
                if (childWidth + lp.getMarginLeft() + lp.getMarginRight() + lineWidth + paddingLeft +
                        paddingRight > width) {
                    mLineIndexes.add(i);
                    mLineHeights.add(lineHeight);
                    lineWidth = childWidth;
                    lineHeight = childHeight;
                } else {
                    /*
                     * 如果不需要换行，则累加
                     */
                    lineWidth += childWidth + lp.getMarginLeft() + lp.getMarginRight();
                    lineHeight = Math.max(lineHeight, childHeight + lp.getMarginTop() + lp.getMarginBottom());
                }
            } else if (lp.positionMode == HNDivLayoutParams.POSITION_ABSOLUTE) {
                child.setComponentPosition(lp.left, lp.top, lp.left + child.getEstimatedWidth(), lp.top + child
                        .getEstimatedHeight());
            }
        }

        mLineHeights.add(lineHeight);
        mLineIndexes.add(childCount);

        int left = paddingLeft;
        int top = paddingTop;

        int line = 0;
        int currentLineIndex = mLineIndexes.get(line);
        lineHeight = mLineHeights.get(line);

        for (int i = 0; i < childCount; i++) {
            if (i >= currentLineIndex) {
                line++;
                currentLineIndex = mLineIndexes.get(line);
                left = paddingLeft;
                top += lineHeight;
                lineHeight = mLineHeights.get(line);
            }

            Component child = getComponentAt(i);
            if (child.getVisibility() == Component.HIDE) {
                continue;
            }
            HNDivLayoutParams lp = (HNDivLayoutParams) child.getLayoutConfig();

            int lc = left + lp.getMarginLeft();
            int tc = top + lp.getMarginTop();
            int rc = lc + child.getEstimatedWidth();
            int bc = tc + child.getEstimatedHeight();

            if (lp.positionMode == HNDivLayoutParams.POSITION_STATIC) {
                child.setComponentPosition(lc, tc, rc, bc);
            } else {
                child.setComponentPosition(lc + lp.left, tc + lp.top, rc, bc);
            }

            left += child.getEstimatedWidth() + lp.getMarginRight() + lp.getMarginLeft();
        }

        top = paddingTop;
        left = paddingLeft;
        int lastLineHeight = 0;
        int right = getEstimatedWidth() - paddingRight;

        lineWidth = getEstimatedWidth() - paddingLeft - paddingRight;
        for (Component v : mFloatViews) {
            if (v.getVisibility() == Component.HIDE) {
                continue;
            }

            HNDivLayoutParams lp = (HNDivLayoutParams) v.getLayoutConfig();

            if (v.getEstimatedWidth() + lp.getMarginRight() + lp.getMarginLeft() > lineWidth) {
                right = getEstimatedWidth() - paddingRight;
                left = paddingLeft;
                top += lastLineHeight;
                lastLineHeight = 0;
                lineWidth = getEstimatedWidth() - paddingLeft - paddingRight;
            }
            if (lp.positionMode == HNDivLayoutParams.POSITION_FLOAT_LEFT) {
                int lc = left + lp.getMarginLeft();
                int tc = top + lp.getMarginTop();
                int rc = lc + v.getEstimatedWidth();
                int bc = tc + v.getEstimatedHeight();
                v.setComponentPosition(lc, tc, rc, bc);
                left += v.getEstimatedWidth() + lp.getMarginLeft() + lp.getMarginRight();

            } else {
                int lc = right - lp.getMarginRight() - v.getEstimatedWidth();
                int tc = top + lp.getMarginTop();
                int rc = right - lp.getMarginRight();
                int bc = tc + v.getEstimatedHeight();
                v.setComponentPosition(lc, tc, rc, bc);

                right -= v.getEstimatedWidth() + lp.getMarginLeft() + lp.getMarginRight();
            }
            lastLineHeight = Math.max(lastLineHeight, v.getHeight() + lp.getMarginTop() + lp
                    .getMarginBottom());
            lineWidth -= v.getEstimatedWidth() - lp.getMarginLeft() - lp.getMarginRight();
        }
        return false;
    }

    public void saveInheritStyles(String styleName, Object style) {
        mSavedInheritStyles.put(styleName, style);
    }

    public Object getInheritStyle(String styleName) {
        return mSavedInheritStyles.get(styleName);
    }

    protected ComponentContainer.LayoutConfig generateDefaultLayoutParams() {
        return new HNDivLayoutParams(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
    }

    public void setHtmlBackground(PixelMap bitmap, Background background) {
        mBackgroundMgr.setHtmlBackground(bitmap, background);
    }

    @Override
    public void setHtmlBackground(Element drawable, Background background) {
        mBackgroundMgr.setHtmlBackground(drawable, background);
    }

    @Override
    public Background getHtmlBackground() {
        return mBackgroundMgr.getHtmlBackground();
    }

    @Override
    public void setBackground(Element background) {
        // don't support the background!! Use setHtmlBackground instead
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mBackgroundMgr.onDraw(canvas);
    }

    protected ComponentContainer.LayoutConfig generateLayoutParams(ComponentContainer.LayoutConfig p) {
        return new ComponentContainer.LayoutConfig(p);
    }

    public ComponentContainer.LayoutConfig generateLayoutParams(AttrSet attrs) {
        return new ComponentContainer.LayoutConfig(getContext(), attrs);
    }

    public static class HNDivLayoutParams extends ComponentContainer.LayoutConfig {

        @IntDef({POSITION_ABSOLUTE, POSITION_RELATIVE, POSITION_STATIC, POSITION_FLOAT_LEFT,
                POSITION_FLOAT_RIGHT})
        @Retention(RetentionPolicy.SOURCE)
        public @interface HNDivPosition {

        }

        public static final int POSITION_STATIC = 0x01;
        public static final int POSITION_RELATIVE = 0x02;
        public static final int POSITION_ABSOLUTE = 0x03;
        public static final int POSITION_FLOAT_LEFT = 0x04;
        public static final int POSITION_FLOAT_RIGHT = 0x05;

        public int left, top, right, bottom;

        @HNDivPosition
        public int positionMode;

        public HNDivLayoutParams(int width, int height) {
            super(width, height);
            this.positionMode = POSITION_STATIC;
        }

//        public HNDivLayoutParams(ComponentContainer.LayoutConfig source) {
//            super(source);
//            this.positionMode = POSITION_STATIC;
//        }

        public HNDivLayoutParams(LayoutConfig source) {
            super(source);
            this.positionMode = POSITION_STATIC;
        }

        public HNDivLayoutParams(HNDivLayoutParams source) {
            super(source);

            this.left = source.left;
            this.top = source.top;
            this.right = source.right;
            this.bottom = source.bottom;
            this.positionMode = source.positionMode;
        }
    }
}
