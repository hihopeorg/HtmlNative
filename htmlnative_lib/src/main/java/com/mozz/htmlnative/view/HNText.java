package com.mozz.htmlnative.view;

import com.mozz.htmlnative.css.Background;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.render.Canvas;
import ohos.app.Context;
import ohos.media.image.PixelMap;

/**
 * @author Yang Tao, 17/5/9.
 */
public class HNText extends Text implements IBackgroundView, Component.DrawTask {
    private BackgroundManager mBackgroundMgr;

    public HNText(Context context) {
        super(context);
        mBackgroundMgr = new BackgroundManager(this);
        init();
    }

    public HNText(Context context, AttrSet attrs) {
        super(context, attrs);
        mBackgroundMgr = new BackgroundManager(this);
        init();
    }

    public HNText(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, String.valueOf(defStyleAttr));
        mBackgroundMgr = new BackgroundManager(this);
        init();
    }


    private void init() {
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mBackgroundMgr.onDraw(canvas);
    }

    @Override
    public void setHtmlBackground(PixelMap bitmap, Background background) {
        mBackgroundMgr.setHtmlBackground(bitmap, background);
    }

    @Override
    public void setHtmlBackground(Element drawable, Background background) {
        mBackgroundMgr.setHtmlBackground(drawable, background);
    }

    @Override
    public Background getHtmlBackground() {
        return mBackgroundMgr.getHtmlBackground();
    }

}
