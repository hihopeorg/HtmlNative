package com.mozz.htmlnative.view;

import com.mozz.htmlnative.css.Background;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Matrix;

/**
 * @author Yang Tao, 17/3/24.
 */

public final class BackgroundViewDelegate {
    private Component mView;
    private Matrix mTransformMatrix;
    private Background mBackground;

    // To indicate whether this image source is going to apply to src of img tag, or just an
    // ordinary background.
    private boolean mIsSrc = false;

    public BackgroundViewDelegate(Component v, Matrix matrix, Background background) {
        this(v, matrix, background, false);
    }

    public BackgroundViewDelegate(Component v, Matrix matrix, Background background, boolean isSrc) {
        mView = v;
        mTransformMatrix = matrix;
        mBackground = background;
        mIsSrc = isSrc;
    }

    public void setBitmap(Element bitmap) {
        if (mView instanceof Image && mIsSrc) {
            Image imageView = (Image) mView;
            HNDivLayout.HNDivLayoutParams params = new HNDivLayout.HNDivLayoutParams(500,500);
            params.setMargins(10, 20, 0, 0);
            imageView.setLayoutConfig(params);
            imageView.setBackground(bitmap);
        } else {
            if (mView instanceof IBackgroundView) {
                ((IBackgroundManager) mView).setHtmlBackground(bitmap, mBackground);
            }
        }
    }
}
