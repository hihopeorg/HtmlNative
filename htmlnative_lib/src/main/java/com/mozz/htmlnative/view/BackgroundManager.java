package com.mozz.htmlnative.view;

import com.mozz.htmlnative.css.Background;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.element.Element;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.media.image.PixelMap;

/**
 * @author Yang Tao, 17/5/9.
 */

class BackgroundManager implements IBackgroundManager {

    private static final String TAG = BackgroundManager.class.getSimpleName();

    private Component mHost;

    private Rect mRect = new Rect();
    private Paint mPaint = new Paint();
    private PixelMap mBackgroundBitmap;
    private Element mBackgroundDrawable;
    private int mLeft, mTop, mWidth, mHeight;
    private int mColorLeft, mColorTop, mColorWidth, mColorHeight;
    private Color mColor = Color.TRANSPARENT;
    private Background mBackground;
    private int mSetBackgroundCount, mMeasureBackgroundCount;

    public BackgroundManager(Component hostView) {
        mHost = hostView;
    }

    @Override
    public void setHtmlBackground(PixelMap bitmap, Background background) {
        if (mHost instanceof ComponentContainer) {
            //mHost.setWillNotDraw(false);//
        }
        mBackgroundBitmap = bitmap;
        mColor = background.getColor();
        mBackground = background;
        mSetBackgroundCount++;

        mHost.invalidate();
    }

    @Override
    public void setHtmlBackground(Element drawable, Background background) {
        mBackgroundDrawable = drawable;
        setHtmlBackground((PixelMap) null, background);
    }

    @Override
    public Background getHtmlBackground() {
        return mBackground;
    }

    /**
     * Should be called at last in {@link //View#onDraw(Canvas)}
     *
     * @param canvas, see {@link //View#onDraw(Canvas)}
     */
    public void onDraw(Canvas canvas) {
        measuredBackground();
        if (mBackground != null && mBackground.isColorSet()) {
            mPaint.setColor(mColor);
            canvas.drawRect(mColorLeft, mColorTop, mColorLeft + mColorWidth, mColorTop +
                    mColorHeight, mPaint);
        }

        if (mBackgroundBitmap != null) {
            mRect.set(mLeft, mTop, mLeft + mWidth, mTop + mHeight);
            canvas.drawPixelMapHolder(new PixelMapHolder(mBackgroundBitmap), mLeft, mTop, mPaint);
        } else if (mBackgroundDrawable != null) {
            mBackgroundDrawable.setBounds(mLeft, mTop, mLeft + mWidth, mTop + mHeight);
            mBackgroundDrawable.drawToCanvas(canvas);
        }
    }

    private void measuredBackground() {
        if (mMeasureBackgroundCount == mSetBackgroundCount) {
            return;
        }

        if (mBackground == null) {
            return;
        }

        if (mBackground.getXMode() == Background.LENGTH) {
            mLeft = (int) mBackground.getX();
        } else {
            mLeft = (int) (mBackground.getX() * mHost.getEstimatedWidth());
        }

        if (mBackground.getYMode() == Background.LENGTH) {
            mTop = (int) mBackground.getY();
        } else {
            mTop = (int) (mBackground.getY() * mHost.getEstimatedHeight());
        }

        if (mBackground.getWidthMode() == Background.LENGTH) {
            mWidth = (int) mBackground.getWidth();
        } else if (mBackground.getWidthMode() == Background.AUTO && mBackgroundBitmap != null) {
            mWidth = mBackgroundBitmap.getImageInfo().size.width;
        } else if (mBackground.getWidthMode() == Background.PERCENTAGE) {
            mWidth = (int) (mBackground.getWidth() * mHost.getEstimatedWidth());
        } else {
            mWidth = mHost.getEstimatedWidth();
        }

        if (mBackground.getHeightMode() == Background.LENGTH) {
            mHeight = (int) mBackground.getHeight();
        } else if (mBackground.getHeightMode() == Background.AUTO && mBackgroundBitmap != null) {
            mHeight = mBackgroundBitmap.getImageInfo().size.height;
        } else if (mBackground.getHeightMode() == Background.PERCENTAGE) {
            mHeight = (int) (mBackground.getHeight() * mHost.getEstimatedHeight());
        } else {
            mHeight = mHost.getEstimatedHeight();
        }

        if (mBackground.getColorWidthMode() == Background.LENGTH) {
            mColorWidth = (int) mBackground.getColorWidth();
        } else if (mBackground.getColorWidthMode() == Background.PERCENTAGE) {
            mColorWidth = (int) (mBackground.getColorWidth() * mHost.getEstimatedWidth());
        } else {
            mColorWidth = mHost.getEstimatedWidth();
        }

        if (mBackground.getColorHeightMode() == Background.LENGTH) {
            mColorHeight = (int) mBackground.getColorHeight();
        } else if (mBackground.getColorHeightMode() == Background.PERCENTAGE) {
            mColorHeight = (int) (mBackground.getColorHeight() * mHost.getEstimatedWidth());
        } else {
            mColorHeight = mHost.getEstimatedWidth();
        }

//        Log.d(TAG, "CalculateResult: mLeft=" + mLeft + ", mTop=" + mTop + ", mWidth=" + mWidth +
//                ", mHeight=" + mHeight + ", mBackground=" + mBackground + ", mColorWidth=" +
//                mColorWidth + ", mColorHeight=" + mColorHeight);

        mMeasureBackgroundCount++;
    }
}
