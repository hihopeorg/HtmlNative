package com.mozz.htmlnative.view;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ScrollView;
import ohos.agp.components.StackLayout;
import ohos.app.Context;
import ohos.utils.LightweightMap;

import java.util.Map;

/**
 * @author Yang Tao, 17/3/8.
 */

public final class HNRootView extends ScrollView {

    private final Map<String, Component> mViewWithId = new LightweightMap<>();

    private static final String TAG = HNRootView.class.getSimpleName();

    private StackLayout mContentView;

    public HNRootView( Context context) {
        super(context);
        mContentView = new StackLayout(context);
        super.addComponent(mContentView, new StackLayout.LayoutConfig(LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_PARENT));
    }

    public void addContent(Component v, ComponentContainer.LayoutConfig layoutParams) {
        mContentView.addComponent(v, layoutParams);
    }

    public Component findViewById( String id) {
        return mViewWithId.get(id);
    }


    public Component putViewWithId(String id, Component view) {
        Component before = mViewWithId.put(id, view);
        if (before != null) {
//            Log.w(TAG, "Duplicated id " + id + ", before is " + before + ", current is " + view);
        }
        return before;
    }

    public String allIdTag() {
        return mViewWithId.toString();
    }

    public boolean containsView(String id) {
        return mViewWithId.containsKey(id);
    }


}
