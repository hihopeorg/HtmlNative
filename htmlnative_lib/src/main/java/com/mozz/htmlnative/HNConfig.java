package com.mozz.htmlnative;

import com.mozz.htmlnative.http.HNHttpClient;
import com.mozz.htmlnative.script.lua.EmptyHttpClient;
import com.mozz.htmlnative.view.BackgroundViewDelegate;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.webengine.WebView;
import ohos.app.Context;
import ohos.utils.net.Uri;

/**
 * @author Yang Tao, 17/6/6.
 */

public class HNConfig {
    static {
        HNRenderer.registerViewFactory(WebView.class.getName(), DefaultWebViewFactory.sInstance);
    }

    private ImageFetcher mImageFetcher = DefaultImageAdapter.sInstance;
    private OnHrefClick mOnHrefClick = DefaultOnHrefClick.sInstance;
    private HNHttpClient mHttpClient = EmptyHttpClient.instance;
    private ScriptCallback mScriptCallback;

    public ImageFetcher getImageViewAdapter() {
        return mImageFetcher;
    }

    public OnHrefClick getHrefLinkHandler() {
        return mOnHrefClick;
    }

    void install() {
        HNScriptRunnerThread.setErrorCallback(mScriptCallback);
    }

    public HNHttpClient getHttpClient() {
        return mHttpClient;
    }

    /**
     * @author Yang Tao, 17/3/11.
     */

    private static final class DefaultOnHrefClick implements OnHrefClick {

        
        static final DefaultOnHrefClick sInstance;

        static {
            sInstance = new DefaultOnHrefClick();
        }

        @Override
        public void onHref(String url,  Component view) {
            Intent secondIntent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withAction("andr"+"oid.intent.action.VIEW")
                    .withFlags(Intent.FLAG_ABILITY_NEW_MISSION)
                    .withUri(Uri.parse(url))
                    .build();
            secondIntent.setOperation(operation);
            view.getContext().startAbility(secondIntent, 0);
        }
    }

    /**
     * @author Yang Tao, 17/3/10.
     */

    private static final class DefaultImageAdapter implements ImageFetcher {

        static DefaultImageAdapter sInstance;

        static {
            sInstance = new DefaultImageAdapter();
        }

        private DefaultImageAdapter() {
        }

        @Override
        public void setImage(String src, BackgroundViewDelegate imageView) {
            //do nothing
        }
    }

    /**
     * @author Yang Tao, 17/3/8.
     */

    private static final class DefaultWebViewFactory implements WebViewFactory {

        static DefaultWebViewFactory sInstance;

        static {
            sInstance = new DefaultWebViewFactory();
        }


        
        @Override
        public WebView create(Context context) {
            return new WebView(context);
        }
    }

    public static class Builder {
        private HNConfig sConfig;

        public Builder() {
            sConfig = new HNConfig();
        }

        public Builder setImageFetcher(ImageFetcher adapter) {
            if (adapter != null) {
                sConfig.mImageFetcher = adapter;
            }
            return this;
        }

        public Builder setOnHrefClick(OnHrefClick onHrefClick) {
            if (onHrefClick != null) {
                sConfig.mOnHrefClick = onHrefClick;
            }
            return this;
        }

        public Builder setHttpClient(HNHttpClient client) {
            if (client != null) {
                sConfig.mHttpClient = client;
            }
            return this;
        }

        public Builder setScriptCallback(ScriptCallback scriptCallback) {
            if (scriptCallback != null) {
                sConfig.mScriptCallback = scriptCallback;
            }

            return this;
        }

        public HNConfig build() {
            return sConfig;
        }
    }
}
