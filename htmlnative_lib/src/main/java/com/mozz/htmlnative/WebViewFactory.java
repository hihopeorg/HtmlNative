package com.mozz.htmlnative;

import ohos.agp.components.webengine.WebView;
import ohos.app.Context;

/**
 * @author Yang Tao, 17/3/8.
 */

public interface WebViewFactory extends HNRenderer.ViewFactory<WebView> {
    WebView create(Context context);
}
