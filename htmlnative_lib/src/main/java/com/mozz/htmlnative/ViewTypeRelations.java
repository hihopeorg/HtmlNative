package com.mozz.htmlnative;

import com.google.harmony.flexbox.FlexboxLayout;
import com.mozz.htmlnative.view.HNDivLayout;
import com.mozz.htmlnative.view.HNImg;
import com.mozz.htmlnative.view.HNText;
import ohos.agp.components.Button;
import ohos.agp.components.ScrollView;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.components.webengine.WebView;
import ohos.utils.LightweightMap;

import java.util.Map;

/**
 * @author Yang Tao, 17/3/3.
 */

public final class ViewTypeRelations {

    static final String BOX = "box";
    static final String LINEAR_BOX = "linear_box";
    static final String FLEX_BOX = "flex_box";

    /**
     * Relate the tag and class.
     */
    private static final Map<String, String> sReservedTagClassTable = new LightweightMap<>();

    /**
     * For extra tag, lazy initialize later.
     */
    private static Map<String, String> sExtraTagClassTable;

    static {
        sReservedTagClassTable.put(BOX, HNDivLayout.class.getName());
        sReservedTagClassTable.put(LINEAR_BOX, HNDivLayout.class.getName());
        sReservedTagClassTable.put(FLEX_BOX, FlexboxLayout.class.getName());

        sReservedTagClassTable.put(HtmlTag.BODY, HNDivLayout.class.getName());
        sReservedTagClassTable.put(HtmlTag.TEMPLATE, HNDivLayout.class.getName());

        sReservedTagClassTable.put(HtmlTag.P, HNText.class.getName());
        sReservedTagClassTable.put(HtmlTag.TEXT, HNText.class.getName());
        sReservedTagClassTable.put(HtmlTag.IMG, HNImg.class.getName());
        sReservedTagClassTable.put(HtmlTag.INPUT, TextField.class.getName());
        sReservedTagClassTable.put(HtmlTag.BUTTON, Button.class.getName());
        sReservedTagClassTable.put(HtmlTag.SCROLLER, ScrollView.class.getName());
        sReservedTagClassTable.put(HtmlTag.IFRAME, WebView.class.getName());
        sReservedTagClassTable.put(HtmlTag.WEB, WebView.class.getName());
        sReservedTagClassTable.put(HtmlTag.A, HNText.class.getName());
        sReservedTagClassTable.put(HtmlTag.SPAN, HNDivLayout.class.getName());
        sReservedTagClassTable.put(HtmlTag.H1, HNText.class.getName());
        sReservedTagClassTable.put(HtmlTag.H2, HNText.class.getName());
        sReservedTagClassTable.put(HtmlTag.H3, HNText.class.getName());
        sReservedTagClassTable.put(HtmlTag.H4, HNText.class.getName());
        sReservedTagClassTable.put(HtmlTag.H5, HNText.class.getName());
        sReservedTagClassTable.put(HtmlTag.H6, HNText.class.getName());

        // for inner element only
        sReservedTagClassTable.put(HtmlTag.INNER_TREE_TAG, Text.class.getName());
    }

    /**
     * Looking for related class name via tag. ViewTypeRelations will first look in
     * {@link ViewTypeRelations#sReservedTagClassTable}, if not found, will continuously look in
     * {@link ViewTypeRelations#sExtraTagClassTable}
     *
     * @param type tag name found in .layout file
     * @return corresponding class name, or null if not found
     */
    public static String findClassByType( String type) {
        String viewClassName = sReservedTagClassTable.get(type.toLowerCase());

        if (viewClassName != null) {
            return viewClassName;
        }

        if (sExtraTagClassTable == null) {
            return null;
        }

        viewClassName = sExtraTagClassTable.get(type);
        if (viewClassName != null) {
            return viewClassName;
        }

        return null;
    }

    static void registerExtraView( String htmlType,  String ohosViewClassName) {
        if (sExtraTagClassTable == null) {
            sExtraTagClassTable = new LightweightMap<>();
        }

        sExtraTagClassTable.put(htmlType, ohosViewClassName);

    }

    static void unregisterExtraView( String htmlType) {
        if (sExtraTagClassTable != null) {
            sExtraTagClassTable.remove(htmlType);
        }
    }

    static void clearAllExtraView() {
        if (sExtraTagClassTable != null) {
            sExtraTagClassTable.clear();
        }
    }
}
