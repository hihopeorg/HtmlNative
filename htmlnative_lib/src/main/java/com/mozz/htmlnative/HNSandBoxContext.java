package com.mozz.htmlnative;

import com.mozz.htmlnative.script.ScriptFactory;
import com.mozz.htmlnative.script.ScriptRunner;
import com.mozz.htmlnative.view.HNRootView;
import ohos.agp.components.Component;
import ohos.app.Context;

/**
 * @author Yang Tao, 17/2/24.
 */
public interface HNSandBoxContext {
    /**
     * get ohos Context
     *
     * @return context, see {@link }
     */
    Context getOhosContext();

    /**
     * to execute script
     *
     * @param script to run
     */
    void execute(String script);

    void executeFun(String funName);

    void executeUIFun(String funName);

    /**
     * Looking for View by Id set in .layout file
     *
     * @param id {@link String}
     * @return View {@link } ,or null if not found
     */
    
    Component findViewById( String id);

    boolean containsView(String id);

    void onViewLoaded();

    void onViewCreate();

    
    Component registerId(String id, Component value);

    String allIdTag();

    HNSegment getSegment();

    void postInScriptThread(Runnable r);
}

/**
 * @author Yang Tao, 17/3/6.
 */
final class HNSandBoxContextImpl implements HNSandBoxContext {

    private static final String TAG = HNSandBoxContext.class.getSimpleName();

    private HNRootView mRootView;

    private ScriptRunner mRunner;

    private final HNSegment mSegment;

    private final Context mContext;

    private HNSandBoxContextImpl(HNSegment segment, Context context, HNRootView rootView) {
        mRootView = rootView;
        mSegment = segment;
        mContext = context;
    }

    @Override
    public Context getOhosContext() {
        return mContext;
    }

    
    public Component registerId(String id, Component view) {
        return mRootView.putViewWithId(id, view);
    }

    
    @Override
    public Component findViewById( String id) {
        return mRootView.findViewById(id);
    }

    @Override
    public boolean containsView(String id) {
        return mRootView.containsView(id);
    }

    @Override
    public void onViewLoaded() {
        callCreated();
    }

    @Override
    public void onViewCreate() {
        // if there is script code in layout file, then initContextScriptRunner
        if (mSegment.hasSetScript()) {
            mRunner = ScriptFactory.createRunner(mSegment.getScriptInfo().type(), this);
            if (mRunner != null) {
                mRunner.attach(this);
                mRunner.onLoad();
            }
        }

        initVariablePool();
        callCreate();
    }

    private void initVariablePool() {

    }

    private void callCreate() {
        if (mSegment.hasSetScript()) {
            execute(mSegment.getScriptInfo().code());
        }
    }

    private void callCreated() {
    }

    public String allIdTag() {
        return mRootView.allIdTag();
    }

    @Override
    public HNSegment getSegment() {
        return mSegment;
    }

    @Override
    public void postInScriptThread(Runnable r) {
        HNScriptRunnerThread.post(r);
    }

    @Override
    public void execute(final String script) {
        if (mRunner == null) {
//            Log.d(TAG, "skip the script \"" + script + "\" because no script in module " +
//                    mSegment);
            return;
        }

        HNScriptRunnerThread.runScript(this, this.mRunner, script);
    }


    @Override
    public void executeFun(final String funName) {
        HNScriptRunnerThread.post(new Runnable() {
            @Override
            public void run() {
                mRunner.runFunction(funName);
            }
        });

    }

    @Override
    public void executeUIFun(final String funName) {
        HNScriptRunnerThread.postAtFront(new Runnable() {
            @Override
            public void run() {
                mRunner.runFunction(funName);
            }
        });
    }

    
    static HNSandBoxContext createContext( HNRootView layout, HNSegment module, Context
            context) {
        return new HNSandBoxContextImpl(module, context, layout);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if (mRunner != null) {
            mRunner.onUnload();
        }
    }
}


