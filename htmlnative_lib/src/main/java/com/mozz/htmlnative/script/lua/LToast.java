package com.mozz.htmlnative.script.lua;

import com.mozz.htmlnative.utils.MainHandlerUtils;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;

import java.lang.ref.WeakReference;

/**
 * @author Yang Tao, 17/2/27.
 */
public class LToast extends OneArgFunction implements ILApi {
    private WeakReference<Context> mContext;

    public LToast(Context context) {
        mContext = new WeakReference<Context>(context);
    }

    @Override
    public LuaValue call( final LuaValue luaValue) {
        MainHandlerUtils.instance().post(new Runnable() {
            @Override
            public void run() {
                Context context = mContext.get();
                if (context != null) {
                    String msg = luaValue.tojstring();
                    new ToastDialog(context).setText(msg).setDuration(1000).show();
                }
            }
        });


        return LuaValue.NIL;
    }

    @Override
    public String apiName() {
        return "toast";
    }
}
