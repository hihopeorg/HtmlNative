package com.mozz.htmlnative.common;

import ohos.aafwk.ability.AbilityPackage;
import ohos.app.Context;

import java.lang.ref.WeakReference;

/**
 * @author Yang Tao, 17/5/26.
 */

public final class ContextProvider {
    private static WeakReference<Context> mApplicationRef;

    public static void install(AbilityPackage application) {
        mApplicationRef = new WeakReference<Context>(application);
    }

    public static Context getApplicationRef() {
        if (mApplicationRef == null) {
            return null;
        }
        return mApplicationRef.get();
    }
}
