package com.mozz.htmlnative.utils;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

public final class MainHandlerUtils {


    private EventHandler mHandler = new EventHandler(EventRunner.getMainEventRunner());

    private MainHandlerUtils() {

    }

    public void postAsynchronous(Runnable r) {

        InnerEvent renderMsg = InnerEvent.get(r);

        mHandler.sendEvent(renderMsg);
    }

    public void postAsynchronousDelay(Runnable r, long delay) {
        mHandler.postTask(r, delay);
    }

    public void post(Runnable r) {
        mHandler.postTask(r);
    }


    private static MainHandlerUtils sInstance = new MainHandlerUtils();


    public static MainHandlerUtils instance() {
        return sInstance;
    }
}
