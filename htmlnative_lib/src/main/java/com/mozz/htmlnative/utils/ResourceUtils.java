package com.mozz.htmlnative.utils;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.lang.reflect.Field;

/**
 * @author Yang Tao, 17/5/25.
 */

public class ResourceUtils {

    private static final String TAG = "ResourceUtils";

    private ResourceUtils() {

    }

    public static String getString(String id, Context context) {
        try {
            Class<?> rClass = getClass(context, ResourceType.String);
            int idd = getId(id, rClass);
            if (idd != Component.ID_DEFAULT) {
                return context.getString(idd);
            } else {
//                Log.e(TAG, "Can't find related string resources of id:" + id);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return null;
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            return null;
        }

        return null;
    }

    public static Color getColor(String id, Context context) {
        try {
            Class<?> rClass = getClass(context, ResourceType.Color);
            int idd = getId(id, rClass);
            if (idd != Component.ID_DEFAULT) {
                return new Color(context.getColor(idd));
            } else {
//                Log.e(TAG, "Can't find related color resources of id:" + id);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return Color.TRANSPARENT;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return Color.TRANSPARENT;
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            return Color.TRANSPARENT;
        }

        return Color.TRANSPARENT;
    }

    public static Element getDrawable(String id, Context context) {
        try {
            Class<?> rClass = getClass(context, ResourceType.Drawable);
            int idd = getId(id, rClass);
            if (idd != Component.ID_DEFAULT) {
                int color = context.getResourceManager().getElement(idd).getColor();
                ShapeElement shapeElement = new ShapeElement();
                shapeElement.setRgbColor(RgbColor.fromArgbInt(color));
                return shapeElement;
            } else {
//                Log.e(TAG, "Can't find related drawable resources of id:" + id);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return null;
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            return null;
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static float getDimension(String id, Context context) {
        try {
            Class<?> rClass = getClass(context, ResourceType.Dimension);
            int idd = getId(id, rClass);
            if (idd != Component.ID_DEFAULT) {
                float aFloat = 0;
                try {
                    aFloat = context.getResourceManager().getElement(idd).getFloat();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NotExistException e) {
                    e.printStackTrace();
                } catch (WrongTypeException e) {
                    e.printStackTrace();
                }
                return aFloat;
            } else {
//                Log.e(TAG, "Can't find related dimension resources of id:" + id);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return 0;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return 0;
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            return 0;
        }

        return 0;
    }

    private static int getId(String id, Class<?> rClass) throws NoSuchFieldException,
            IllegalAccessException {
        Field f = rClass.getField(id);
        if (f != null) {
            Object r = f.get(rClass);
            if (r instanceof Integer) {
                return (Integer) r;
            }
        }

        return Component.ID_DEFAULT;
    }

    private static Class<?> getClass(Context context, ResourceType type) throws
            ClassNotFoundException {

        switch (type) {
            case String:
                return getRStringClass(context);
            case Color:
                return getRColorClass(context);
            case Drawable:
                return getRDrawableClass(context);
            case Dimension:
                return getRDimensionClass(context);
        }

        return null;
    }

    private static Class<?> getRStringClass(Context context) throws ClassNotFoundException {
        return Class.forName(context.getBundleName() + ".R$string");
    }

    private static Class<?> getRColorClass(Context context) throws ClassNotFoundException {
        return Class.forName(context.getBundleName() + ".R$color");
    }

    private static Class<?> getRDrawableClass(Context context) throws ClassNotFoundException {
        return Class.forName(context.getBundleName() + ".R$drawable");
    }

    private static Class<?> getRDimensionClass(Context context) throws ClassNotFoundException {
        return Class.forName(context.getBundleName() + ".R$dimen");
    }

    private enum ResourceType {
        String, Color, Drawable, Dimension
    }
}
