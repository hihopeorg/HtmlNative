package com.mozz.htmlnative;

import com.mozz.htmlnative.css.AttrsSet;
import com.mozz.htmlnative.css.InheritStylesRegistry;
import com.mozz.htmlnative.css.StyleSheet;
import com.mozz.htmlnative.css.Styles;
import com.mozz.htmlnative.css.selector.CssSelector;
import com.mozz.htmlnative.css.stylehandler.LayoutStyleHandler;
import com.mozz.htmlnative.css.stylehandler.StyleHandler;
import com.mozz.htmlnative.css.stylehandler.StyleHandlerFactory;
import com.mozz.htmlnative.dom.AttachedElement;
import com.mozz.htmlnative.dom.DomElement;
import com.mozz.htmlnative.dom.HNDomTree;
import com.mozz.htmlnative.exception.AttrApplyException;
import com.mozz.htmlnative.view.HNRootView;
import com.mozz.htmlnative.view.LayoutParamsCreator;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;
import ohos.miscservices.timeutility.Time;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.mozz.htmlnative.ViewTypeRelations.*;
import static com.mozz.htmlnative.css.Styles.ATTR_DISPLAY;

/**
 * Render views
 */
public final class HNRenderer {

    /**
     * cache the constructor for later use
     */
    private static final HashMap<String, Constructor<? extends Component>> sConstructorMap = new
            HashMap<>();

    private static final Map<String, ViewFactory> sViewFactory = new HashMap<>();

    private static final Object[] sConstructorArgs = new Object[1];

    private static final Class<?>[] sConstructorSignature = new Class[]{Context.class};

    private InheritStyleStack mInheritStyleStack;

    private Tracker mTracker;

    private HNRenderer() {
        mInheritStyleStack = new InheritStyleStack();
        mTracker = new Tracker();
    }

    
    public static HNRenderer get() {
        return new HNRenderer();
    }

    public static InheritStyleStack computeInheritStyle(Component view) {

        StyleHandler viewStyleHandler = StyleHandlerFactory.get(view);
        StyleHandler extraStyleHandler = StyleHandlerFactory.extraGet(view);
        LayoutStyleHandler parentLayoutAttr = StyleHandlerFactory.parentGet(view);

        InheritStyleStack inheritStyleStack = new InheritStyleStack();
        inheritStyleStack.push();

        Iterator<String> itr = InheritStylesRegistry.iterator();

        while (itr.hasNext()) {
            String params = itr.next();

            Object val = Styles.getStyle(view, params, viewStyleHandler, extraStyleHandler,
                    parentLayoutAttr);
            if (val != null) {
                inheritStyleStack.newStyle(params, val);
            }
        }

        return inheritStyleStack;
    }

    final Component render( Context context,  HNSegment segment) throws
            HNRenderException {

//        Trace.beginSection("NHRenderer render start");

        mTracker.reset();

        HNLog.d(HNLog.RENDER, "start to render " + segment.toString());
        HNRootView rootViewGroup = new HNRootView(context);

        HNSandBoxContext sandBoxContext = HNSandBoxContextImpl.createContext(rootViewGroup,
                segment, context);


        mInheritStyleStack.reset();

        LayoutParamsCreator rootCreator = new LayoutParamsCreator();

        long renderStartTime = Time.getCurrentThreadTime();
        Component v = renderInternal(context, sandBoxContext, segment.getDom(), segment,
                rootViewGroup, rootCreator, rootViewGroup, segment.getStyleSheet());


        if (v != null) {
            rootViewGroup.addContent(v, LayoutParamsCreator.createLayoutParams(rootViewGroup,
                    rootCreator));
            mTracker.record("Render View", Time.getCurrentThreadTime() - renderStartTime);

            long createTime = Time.getCurrentThreadTime();
            this.performCreate(sandBoxContext);
            mTracker.record("Create View", Time.getCurrentThreadTime() - createTime);

            long afterCreate = Time.getCurrentThreadTime();
            this.performCreated(sandBoxContext);
            mTracker.record("After View Created", Time.getCurrentThreadTime() -
                    afterCreate);

//            Log.i(PERFORMANCE_TAG, mTracker.dump());

            HNLog.d(HNLog.RENDER, sandBoxContext.allIdTag());
//            Trace.endSection();
            return rootViewGroup;
        }

//        Trace.endSection();
        return null;
    }

    private Component renderInternal( Context context,  HNSandBoxContext
            sandBoxContext, HNDomTree dom, HNSegment segment,  ComponentContainer parent, 
            LayoutParamsCreator paramsCreator,  HNRootView root, StyleSheet styleSheet)
            throws HNRenderException {

        AttrsSet attrsSet = segment.getInlineStyles();

        if (dom.isLeaf()) {
            Component v = createView(dom, dom, sandBoxContext, parent, context, attrsSet,
                    paramsCreator, styleSheet, mInheritStyleStack);
            mInheritStyleStack.pop();
            return v;
        } else {
            Component view = createView(dom, dom, sandBoxContext, parent, context, attrsSet,
                    paramsCreator, styleSheet, mInheritStyleStack);

            if (view == null) {
                return null;
            }


            if (view instanceof ComponentContainer) {

                final ComponentContainer viewGroup = (ComponentContainer) view;

                List<HNDomTree> children = dom.children();
                for (HNDomTree child : children) {

                    LayoutParamsCreator childCreator = new LayoutParamsCreator();

                    // Recursively render child.
                    final Component v = renderInternal(context, sandBoxContext, child, segment,
                            viewGroup, childCreator, root, styleSheet);

                    if (v != null) {
                        addView(viewGroup, v, childCreator);
                    } else {
                        HNLog.e(HNLog.RENDER, "error when inflating " + child.getType());
                    }
                }
            } else {
                HNLog.e(HNLog.RENDER, "View render from HNRenderer is not " + "an " + "viewGroup"
                        + view.getClass().getSimpleName() + ", but related HNDomTree has " +
                        "children" + ". Will ignore its children!");
            }

            mInheritStyleStack.pop();
            return view;
        }
    }


    public static Component createView(AttrsSet.AttrsOwner owner,  DomElement element,
                                   HNSandBoxContext sandBoxContext, ComponentContainer parent,
                                   Context context, AttrsSet attrsSet, 
                                          LayoutParamsCreator layoutCreator, StyleSheet
                                          styleSheet, InheritStyleStack stack) throws
            HNRenderException {

        String type = element.getType();

        if (stack != null) {
            stack.push();
        }

        try {
            Component v;
            if (HtmlTag.isGroupingElement(type)) {
                v = createOhosViewGroup(context, type, owner, attrsSet, layoutCreator);
            } else {
                v = createOhosView(context, type);
            }

            if (v == null) {
                HNLog.e(HNLog.RENDER, "createView createDiv: view is null with tag " + type);
                return null;
            }

            //attach the dom element to view
            DomElement domElement = AttachedElement.cloneIfNecessary(element);
            domElement.setParent((DomElement) parent.getTag());
            v.setTag(AttachedElement.cloneIfNecessary(element));


            // save the id if element has one
            String id = element.getId();
            if (id != null) {
                sandBoxContext.registerId(id, v);
            }

            // ------- below starts the styleSheet process part -------

            // 1 - find the related StyleHandler

            StyleHandler viewStyleHandler = StyleHandlerFactory.get(v);
            StyleHandler extraStyleHandler = StyleHandlerFactory.extraGet(v);
            LayoutStyleHandler parentLayoutAttr = StyleHandlerFactory.parentGet(v);

            // 2 - set initial style to an view
            try {
                Styles.setDefaultStyle(context, sandBoxContext, v, element, parent,
                        viewStyleHandler, extraStyleHandler, parentLayoutAttr, layoutCreator);
            } catch (AttrApplyException e) {
                e.printStackTrace();
            }

            // 3 - use parent inherit style
            try {
                /*
                 * First apply the parent styleSheet style to it.
                 */
                if (stack != null) {
                    for (Styles.StyleEntry entry : stack) {

                        // here pass InheritStyleStack null to Styles, is to prevent Style being
                        // stored in InheritStyleStack twice
                        Styles.applySingleStyle(context, sandBoxContext, v, element,
                                layoutCreator, parent, viewStyleHandler, extraStyleHandler,
                                parentLayoutAttr, entry, null);

                    }
                }
            } catch (AttrApplyException e) {
                e.printStackTrace();
                HNLog.e(HNLog.RENDER, "wrong when apply inherit attr to " + type);
            }

            // 4 - use CSS to render
            if (styleSheet != null) {
                CssSelector[] matchedSelectors = styleSheet.matchedSelector(type, element.getId()
                        , element.getClazz());

                for (CssSelector selector : matchedSelectors) {
                    if (selector != null) {
                        if (selector.matchWhole(element)) {

                            try {
                                Styles.applyStyles(context, sandBoxContext, styleSheet, v,
                                        selector, element, parent, layoutCreator,
                                        viewStyleHandler, extraStyleHandler, parentLayoutAttr,
                                        stack);

                            } catch (AttrApplyException e) {
                                e.printStackTrace();
                                HNLog.e(HNLog.RENDER, "Wrong when apply css style to " + type);
                            }
                        }
                    }
                }
            }

            // 5 - use inline-style to render
            try {
                if (attrsSet != null) {
                    Styles.applyStyles(context, sandBoxContext, attrsSet, v, owner, element,
                            parent, layoutCreator, viewStyleHandler, extraStyleHandler,
                            parentLayoutAttr, stack);
                }
            } catch (AttrApplyException e) {
                e.printStackTrace();
                HNLog.e(HNLog.RENDER, "wrong when apply inline attr to " + type);
            }
            return v;

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new HNRenderException("class not found " + type);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            throw new HNRenderException("class's constructor is missing " + type);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new HNRenderException("class's constructor can not be accessed " + type);
        } catch (InstantiationException e) {
            e.printStackTrace();
            throw new HNRenderException("class's constructor can not be invoked " + type);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            throw new HNRenderException("class's method has something wrong " + type);
        }
    }

    
    static Component createOhosView( Context context,  String typeName) throws
            ClassNotFoundException, NoSuchMethodException, IllegalAccessException,
            InvocationTargetException, InstantiationException {

        String viewClassName = ViewTypeRelations.findClassByType(typeName);
        if (viewClassName == null) {
            return null;
        }

        HNLog.d(HNLog.RENDER, "createContext view" + viewClassName + " with type" + typeName);

        // first let viewFactory to hook the create process
        Component view = createViewByViewFactory(context, viewClassName);
        if (view != null) {
            return view;
        }

        Constructor<? extends Component> constructor = sConstructorMap.get(viewClassName);
        Class<? extends Component> clazz;
        if (constructor == null) {
            // Class not found in the cache, see if it's real, and try to add it
            clazz = context.getClassloader().loadClass(viewClassName).asSubclass(Component.class);
            constructor = clazz.getConstructor(sConstructorSignature);
            constructor.setAccessible(true);
            sConstructorMap.put(viewClassName, constructor);
        }

        sConstructorArgs[0] = context;
        view = constructor.newInstance(sConstructorArgs);
        // release the context
        sConstructorArgs[0] = null;
        return view;
    }

    static Component createOhosViewGroup( Context context,  String typeName,
                                       AttrsSet.AttrsOwner owner, AttrsSet attrsSet,
                                       LayoutParamsCreator layoutParamsCreator) throws
            ClassNotFoundException, NoSuchMethodException, InvocationTargetException,
            InstantiationException, IllegalAccessException {

        // set the <body> width to 100%
        layoutParamsCreator.width = ComponentContainer.LayoutConfig.MATCH_PARENT;
        layoutParamsCreator.width = ComponentContainer.LayoutConfig.MATCH_CONTENT;

        if (attrsSet != null) {
            Object displayObj = attrsSet.getStyle(owner, ATTR_DISPLAY);
            if (displayObj != null && displayObj instanceof String) {
                String display = (String) displayObj;
                switch (display) {
                    case Styles.VAL_DISPLAY_FLEX:
                        return createOhosView(context, FLEX_BOX);
                    case Styles.VAL_DISPLAY_ABSOLUTE:
                        return createOhosView(context, BOX);

                    case Styles.VAL_DISPLAY_BOX:
                    default:
                        return createOhosView(context, LINEAR_BOX);
                }
            }
        }

        return createOhosView(context, LINEAR_BOX);
    }

    public static void renderStyle(Context context, final HNSandBoxContext sandBoxContext, Component
            v, DomElement domElement,  LayoutParamsCreator layoutCreator, 
            ComponentContainer parent, String styleName, Object style, boolean isParent, InheritStyleStack
            stack) throws AttrApplyException {

        StyleHandler viewStyleHandler = StyleHandlerFactory.get(v);
        StyleHandler extraStyleHandler = StyleHandlerFactory.extraGet(v);
        LayoutStyleHandler parentLayoutAttr = StyleHandlerFactory.parentGet(v);

        Styles.applySingleStyle(context, sandBoxContext, v, domElement, layoutCreator, parent,
                viewStyleHandler, extraStyleHandler, parentLayoutAttr, styleName, style, stack);
    }

    public static void renderStyle(Context context,  final HNSandBoxContext
            sandBoxContext,  Component v, DomElement domElement,  LayoutParamsCreator
            layoutCreator, ComponentContainer parent,  Map<String, Object> styles,
                                   InheritStyleStack stack) throws AttrApplyException {

        final StyleHandler viewStyleHandler = StyleHandlerFactory.get(v);
        final StyleHandler extraStyleHandler = StyleHandlerFactory.extraGet(v);
        final LayoutStyleHandler parentAttr = StyleHandlerFactory.parentGet(v);

        for (Map.Entry<String, Object> entry : styles.entrySet()) {

            try {
                Styles.applySingleStyle(v.getContext(), sandBoxContext, v, domElement,
                        layoutCreator, parent, viewStyleHandler, extraStyleHandler, parentAttr,
                        entry.getKey(), entry.getValue(), stack);


            } catch (AttrApplyException e) {
                e.printStackTrace();
            }
        }
    }

    public static void addView(ComponentContainer parent, Component v, LayoutParamsCreator creator) {
        if (parent == null || v == null) {
            HNLog.e(HNLog.RENDER, "Wrong when trying to add " + v + " to " + parent + " with " +
                    "creator " + creator);
            return;
        }
        parent.addComponent(v, LayoutParamsCreator.createLayoutParams(parent, creator));
    }

    public static void addView(ComponentContainer parent, Component v, LayoutParamsCreator creator, int index) {
        if (parent == null || v == null) {
            HNLog.e(HNLog.RENDER, "Wrong when trying to add " + v + " to " + parent + " with " +
                    "creator " + creator);
            return;
        }
        parent.addComponent(v, index, LayoutParamsCreator.createLayoutParams(parent, creator));
    }

    public static void registerViewFactory(String ohosClassName, ViewFactory factory) {
        sViewFactory.put(ohosClassName, factory);
    }

    public static void unregisterViewFactory(String ohosClassName) {
        sViewFactory.remove(ohosClassName);
    }

    public static void clearAllViewFactory() {
        sViewFactory.clear();
    }

    private static Component createViewByViewFactory(Context context,  String viewClassName) {
        ViewFactory factory = sViewFactory.get(viewClassName);
        if (factory != null) {
            return factory.create(context);
        }

        return null;
    }


    private void performCreate(HNSandBoxContext sandBoxContext) {
        if (sandBoxContext != null) {
            sandBoxContext.onViewCreate();
        }
    }

    private void performCreated(HNSandBoxContext sandBoxContext) {
        if (sandBoxContext != null) {
            sandBoxContext.onViewLoaded();
        }
    }


    public static class HNRenderException extends Exception {
        public HNRenderException() {
            super();
        }

        public HNRenderException(String detailMessage, Throwable throwable) {
            super(detailMessage, throwable);
        }

        public HNRenderException(String detailMessage) {
            super(detailMessage);
        }

        public HNRenderException(Throwable throwable) {
            super(throwable);
        }
    }

    /**
     * Used to hook the view creation process.
     *
     * @author Yang Tao, 17/3/8.
     */

    public interface ViewFactory<T extends Component> {
        T create(Context context);
    }
}
