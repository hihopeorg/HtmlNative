package com.mozz.htmlnative.css.stylehandler;

import com.mozz.htmlnative.HtmlTag;
import com.mozz.htmlnative.common.PixelValue;
import com.mozz.htmlnative.css.InheritStylesRegistry;
import com.mozz.htmlnative.dom.DomElement;
import com.mozz.htmlnative.exception.AttrApplyException;
import com.mozz.htmlnative.utils.ParametersUtils;
import com.mozz.htmlnative.utils.TextUtils;
import com.mozz.htmlnative.view.LayoutParamsCreator;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.text.Font;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;

import static com.mozz.htmlnative.utils.ParametersUtils.*;

class TextViewStyleHandler extends StyleHandler {

    private static final String FONT_SIZE = "font-size";
    private static final String COLOR = "color";
    private static final String TEXT = "text";
    private static final String LINE_HEIGHT = "getLine-height";
    private static final String FONT_STYLE = "font-style";
    private static final String FONT_WEIGHT = "font-weight";
    private static final String TEXT_ALIGN = "text-align";
    private static final String TEXT_WORD_SPACING = "word-spacing";
    private static final String TEXT_OVER_FLOW = "text-overflow";
    private static final String TEXT_TRANSFORM = "text-transform";

    private static final int DEFAULT_SIZE = 44;
    private static final int DEFAULT_H1_SIZE = emToPx(2);
    private static final int DEFAULT_H1_PADDING = emToPx(0.67f);

    private static final int DEFAULT_H2_SIZE = emToPx(1.5f);
    private static final int DEFAULT_H2_PADDING = (int) emToPx(.75f);

    private static final int DEFAULT_H3_SIZE = (int) emToPx(1.17f);
    private static final int DEFAULT_H3_PADDING = (int) emToPx(.83f);

    private static final int DEFAULT_H4_SIZE = (int) emToPx(1.f);
    private static final int DEFAULT_H4_PADDING = (int) emToPx(1.12f);

    private static final int DEFAULT_H5_SIZE = (int) emToPx(0.8f);
    private static final int DEFAULT_H5_PADDING = (int) emToPx(1.12f);

    private static final int DEFAULT_H6_SIZE = (int) emToPx(.6f);
    private static final int DEFAULT_H6_PADDING = (int) emToPx(1.12f);

    private static final int DEFAULT_P_PADDING = (int) dpToPx(5);

    static {
        InheritStylesRegistry.register(FONT_SIZE);
        InheritStylesRegistry.register(COLOR);
        InheritStylesRegistry.register(LINE_HEIGHT);
        InheritStylesRegistry.register(FONT_STYLE);
        InheritStylesRegistry.register(FONT_WEIGHT);
        InheritStylesRegistry.register(TEXT_ALIGN);
        InheritStylesRegistry.register(TEXT_WORD_SPACING);
        InheritStylesRegistry.register(TEXT_TRANSFORM);

        // to protect the build-in styles
        InheritStylesRegistry.preserve(TEXT);
        InheritStylesRegistry.preserve(TEXT_OVER_FLOW);
    }

    @Override
    public void apply(Context context, Component v, DomElement domElement, Component parent,
                      LayoutParamsCreator paramsCreator, String params, final Object
                              value) throws AttrApplyException {

        final Text textView = (Text) v;
        switch (params) {
            case COLOR:
                try {
                    textView.setTextColor(toColor(value));
                } catch (ParametersUtils.ParametersParseException e) {
                    e.printStackTrace();
                }
                break;

            case TEXT:
                textView.setText(value.toString());
                textView.setMultipleLine(true);
                break;

            case FONT_SIZE:
                try {
                    PixelValue size;
                    size = toPixel(value);
                    textView.setTextSize(size.getUnit());
                } catch (ParametersUtils.ParametersParseException e) {
                    e.printStackTrace();
                }
                break;

            case LINE_HEIGHT:
                try {
                    if (value instanceof String) {
                        if (((String) value).endsWith("%")) {
                            float percent = ParametersUtils.getPercent((String) value);
                            textView.setLineSpacing(0, percent);
                        } else {
                            float lineHeight;
                            lineHeight = toPixel(value).getPxValue();
                            textView.setLineSpacing(lineHeight, 0);
                        }
                    }
                } catch (ParametersUtils.ParametersParseException e) {
                    e.printStackTrace();
                }
                break;

            case FONT_WEIGHT:
                String s = value.toString();

                if (s.equals("bold")) {
                    textView.setFont(Font.DEFAULT_BOLD);
                } else if (s.equals("normal")) {
                    textView.setFont(Font.DEFAULT);
                }

                break;

            case FONT_STYLE:
                String s2 = value.toString();

                if (s2.equals("italic")) {
                    Font style = textView.getFont();
                    if (style == Font.DEFAULT_BOLD) {
                        style = Font.SERIF;
                    } else {
                        style = Font.SERIF;
                    }

                    textView.setFont(style);
                } else if (s2.equals("normal")) {
                    Font style = textView.getFont();
                    if (style == Font.SERIF) {
                        style = Font.DEFAULT_BOLD;
                    } else {
                        style = Font.DEFAULT;
                    }

                    textView.setFont(style);
                }

                break;


            case TEXT_ALIGN:
                String val = value.toString();
                switch (val) {
                    case "center":
                        textView.setTextAlignment(TextAlignment.CENTER);
                        break;
                    case "left":
                        textView.setTextAlignment(TextAlignment.START);
                        break;
                    case "right":
                        textView.setTextAlignment(TextAlignment.END);
                        break;
                }

                break;

            case TEXT_WORD_SPACING: {
                String ss = value.toString();
                if (ss.equals("normal")) {
//                    textView.setLetterSpacing(textView.getLetterSpacing());//字体间距
//                    Paint paint = new Paint();
//                    paint.setLetterSpacing();
                } else {
                    try {
                        PixelValue f = toPixel(value);
//                        textView.setLetterSpacing(f.getEmValue());
/*                        Paint paint = new Paint();
                        paint.setLetterSpacing(f.getEmValue());
                        Canvas canvas = new Canvas();
                        canvas.drawText(paint, textView.getText(), textView.getScaleX(), textView.getScaleY());*/
                    } catch (ParametersUtils.ParametersParseException e) {
                        e.printStackTrace();
                    }
                }
                break;
            }

            case TEXT_OVER_FLOW: {
                String ss = value.toString();

                if (ss.equals("ellipsis")) {
                    textView.setTruncationMode(Text.TruncationMode.ELLIPSIS_AT_END);
                }
                break;
            }

            case TEXT_TRANSFORM: {
                switch (value.toString()) {
                    case "uppercase":
                        String text = textView.getText();
                        text = text.toUpperCase();
                        textView.setText(text);
                        break;
                    case "lowercase":
                        String text1 = textView.getText();
                        text1 = text1.toLowerCase();
                        textView.setText(text1);
                        break;
                }
            }
            break;
        }

    }

    @Override
    public void setDefault(Context context, Component v, DomElement domElement,
                           LayoutParamsCreator paramsCreator, Component parent) throws
            AttrApplyException {

        Text textView = (Text) v;

        if (!TextUtils.isEmpty(domElement.getInner()) && TextUtils.isEmpty(textView.getText())) {
            textView.setText(String.valueOf(domElement.getInner()));
        }

        textView.setTextSize(DEFAULT_SIZE);

        switch (domElement.getType()) {
            case HtmlTag.H1:
                textView.setTextSize(DEFAULT_H1_SIZE);
                paramsCreator.width = ComponentContainer.LayoutConfig.MATCH_PARENT;
                StyleHelper.setPadding(textView, DEFAULT_H1_PADDING, textView.getPaddingLeft(),
                        DEFAULT_H1_PADDING, textView.getPaddingRight());
                StyleHelper.setBold(textView);
                break;

            case HtmlTag.H2:
                textView.setTextSize(DEFAULT_H2_SIZE);
                paramsCreator.width = ComponentContainer.LayoutConfig.MATCH_PARENT;
                StyleHelper.setPadding(textView, DEFAULT_H2_PADDING, textView.getPaddingLeft(),
                        DEFAULT_H2_PADDING, textView.getPaddingRight());
                StyleHelper.setBold(textView);
                break;

            case HtmlTag.H3:
                textView.setTextSize(DEFAULT_H3_SIZE);
                paramsCreator.width = ComponentContainer.LayoutConfig.MATCH_PARENT;
                StyleHelper.setPadding(textView, DEFAULT_H3_PADDING, textView.getPaddingLeft(),
                        DEFAULT_H3_PADDING, textView.getPaddingRight());
                StyleHelper.setBold(textView);
                break;

            case HtmlTag.H4:
                textView.setTextSize(DEFAULT_H4_SIZE);
                paramsCreator.width = ComponentContainer.LayoutConfig.MATCH_PARENT;
                StyleHelper.setPadding(textView, DEFAULT_H4_PADDING, textView.getPaddingLeft(),
                        DEFAULT_H4_PADDING, textView.getPaddingRight());
                StyleHelper.setBold(textView);
                break;

            case HtmlTag.H5:
                textView.setTextSize(DEFAULT_H5_SIZE);
                paramsCreator.width = ComponentContainer.LayoutConfig.MATCH_PARENT;
                StyleHelper.setPadding(textView, DEFAULT_H5_PADDING, textView.getPaddingLeft(),
                        DEFAULT_H5_PADDING, textView.getPaddingRight());
                StyleHelper.setBold(textView);
                break;

            case HtmlTag.H6:
                textView.setTextSize(DEFAULT_H6_SIZE);
                paramsCreator.width = ComponentContainer.LayoutConfig.MATCH_PARENT;
                StyleHelper.setPadding(textView, DEFAULT_H6_PADDING, textView.getPaddingLeft(),
                        DEFAULT_H6_PADDING, textView.getPaddingRight());
                StyleHelper.setBold(textView);
                break;

            case HtmlTag.P:
                StyleHelper.setTopPadding(textView, DEFAULT_P_PADDING);
                StyleHelper.setBottomPadding(textView, DEFAULT_P_PADDING);
                paramsCreator.width = ComponentContainer.LayoutConfig.MATCH_PARENT;
                break;
            case HtmlTag.A:
                StyleHelper.setUnderLine(textView);
                paramsCreator.width = ComponentContainer.LayoutConfig.MATCH_CONTENT;
                break;
        }
    }

    @Override
    public Object getStyle(Component v, String styleName) {
        final Text textView = (Text) v;
        switch (styleName) {
            case COLOR:
                return "#ff0000";
        }
        //TODO
        return null;
    }
}
