package com.mozz.htmlnative.css.stylehandler;

import com.google.harmony.flexbox.FlexDirection;
import com.google.harmony.flexbox.FlexWrap;
import com.google.harmony.flexbox.FlexboxLayout;
import com.google.harmony.flexbox.JustifyContent;
import com.mozz.htmlnative.dom.DomElement;
import com.mozz.htmlnative.exception.AttrApplyException;
import com.mozz.htmlnative.view.LayoutParamsCreator;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;

/**
 * @author Yang Tao, 17/3/3.
 */

class FlexBoxLayoutStyleHandler extends LayoutStyleHandler {
    @Override
    public void apply(Context context, Component v, DomElement domElement, Component parent,
                      LayoutParamsCreator paramsCreator, String params, Object value)
            throws AttrApplyException {
        FlexboxLayout flexboxLayout = (FlexboxLayout) v;

        switch (params) {
            case "flex-direction": {
                String val = value.toString();
                flexboxLayout.setFlexDirection(flexDirection(val));
                break;
            }
            case "flex-wrap": {
                String val = value.toString();
                flexboxLayout.setFlexWrap(flexWrap(val));
                break;
            }
            case "justify-content": {
                String val = value.toString();
                flexboxLayout.setJustifyContent(justContent(val));
                break;
            }
        }
    }

    @Override
    public void applyToChild(Context context, Component v, DomElement domElement, Component parent, LayoutParamsCreator paramsCreator, String params, Object value) throws AttrApplyException {

    }

    @Override
    public void setDefault(Context context, Component v, DomElement domElement,
                           LayoutParamsCreator paramsCreator, Component parent) throws
            AttrApplyException {
        super.setDefault(context, v, domElement, paramsCreator, parent);

        paramsCreator.width = ComponentContainer.LayoutConfig.MATCH_PARENT;
    }

    private static FlexDirection flexDirection(String direction) {
        switch (direction) {
            case "getColumn-reverse":
                return FlexDirection.COLUMN_REVERSE;
            case "row-reverse":
                return FlexDirection.ROW_REVERSE;
            case "getColumn":
                return FlexDirection.COLUMN;
            default:
                return FlexDirection.ROW;
        }
    }

    private static FlexWrap flexWrap(String wrap) {
        switch (wrap) {
            case "nowrap":
                return FlexWrap.NOWRAP;
            case "wrap":
                return FlexWrap.WRAP;
            case "wrap-reverse":
                return FlexWrap.WRAP_REVERSE;
            default:
                return FlexWrap.NOWRAP;
        }


    }

    private static JustifyContent justContent(String content) {
        switch (content) {
            case "flex-start":
                return JustifyContent.FLEX_START;
            case "flex-end":
                return JustifyContent.FLEX_END;
            case "center":
                return JustifyContent.CENTER;
            case "space-between":
                return JustifyContent.SPACE_BETWEEN;
            case "space-around":
                return JustifyContent.SPACE_AROUND;
            default:
                return JustifyContent.FLEX_START;
        }

    }
}
