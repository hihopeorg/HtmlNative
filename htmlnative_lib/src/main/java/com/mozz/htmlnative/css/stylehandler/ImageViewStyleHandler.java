package com.mozz.htmlnative.css.stylehandler;

import com.mozz.htmlnative.HNativeEngine;
import com.mozz.htmlnative.css.Background;
import com.mozz.htmlnative.css.InheritStylesRegistry;
import com.mozz.htmlnative.dom.DomElement;
import com.mozz.htmlnative.exception.AttrApplyException;
import com.mozz.htmlnative.view.BackgroundViewDelegate;
import com.mozz.htmlnative.view.LayoutParamsCreator;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.utils.Matrix;
import ohos.app.Context;

class ImageViewStyleHandler extends StyleHandler {

    static {
        // to protect the build-in styles
        InheritStylesRegistry.preserve("src");
    }

    @Override
    public void apply(Context context, Component v, DomElement domElement, Component parent,
                      LayoutParamsCreator paramsCreator, String params, Object value)
            throws AttrApplyException {
        if (params.equals("src") && HNativeEngine.getImageViewAdapter() != null) {
            Matrix matrix = null;
            String url = value.toString();
            Background background = null;
            if (value instanceof Background) {
                matrix = Background.createBitmapMatrix((Background) value);
                url = ((Background) value).getUrl();
                background = (Background) value;
            }

            HNativeEngine.getImageViewAdapter().setImage(url, new BackgroundViewDelegate(v,
                    matrix, background, true));
        }
    }

    @Override
    public void setDefault(Context context, Component v, DomElement domElement,
                           LayoutParamsCreator paramsCreator, Component parent) throws
            AttrApplyException {
        super.setDefault(context, v, domElement, paramsCreator, parent);

        paramsCreator.width = ComponentContainer.LayoutConfig.MATCH_PARENT;
    }
}
