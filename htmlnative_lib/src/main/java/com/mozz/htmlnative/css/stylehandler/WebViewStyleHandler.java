package com.mozz.htmlnative.css.stylehandler;

import com.mozz.htmlnative.css.InheritStylesRegistry;
import com.mozz.htmlnative.dom.DomElement;
import com.mozz.htmlnative.exception.AttrApplyException;
import com.mozz.htmlnative.view.LayoutParamsCreator;
import ohos.agp.components.Component;
import ohos.agp.components.webengine.WebView;
import ohos.app.Context;

/**
 * @author Yang Tao, 17/3/6.
 */

class WebViewStyleHandler extends StyleHandler {

    private static final String ATTR_SRC = "src";

    static {
        // to protect the build-in styles
        InheritStylesRegistry.preserve(ATTR_SRC);
    }

    @Override
    public void apply(Context context, Component v, DomElement domElement, Component parent,
                      LayoutParamsCreator paramsCreator, String params, Object value)
            throws AttrApplyException {
        final WebView webView = (WebView) v;

        if (params.equals(ATTR_SRC)) {
            webView.load(value.toString());
        }
    }
}
