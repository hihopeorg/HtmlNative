package com.mozz.htmlnative.css.stylehandler;

import com.mozz.htmlnative.dom.DomElement;
import com.mozz.htmlnative.exception.AttrApplyException;
import com.mozz.htmlnative.view.LayoutParamsCreator;
import ohos.agp.components.Component;
import ohos.app.Context;

/**
 * @author Yang Tao, 17/2/22.
 */

public abstract class StyleHandler {
    public abstract void apply(Context context, Component v, DomElement domElement, Component parent,
                               LayoutParamsCreator paramsCreator, String params, Object
                                       value) throws AttrApplyException;

    public void setDefault(Context context, Component v, DomElement domElement,
                           LayoutParamsCreator paramsCreator, Component parent) throws
            AttrApplyException {

    }

    public Object getStyle(Component v, String styleName) {
        return null;
    }
}
