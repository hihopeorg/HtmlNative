package com.mozz.htmlnative.css.stylehandler;

import com.mozz.htmlnative.dom.DomElement;
import com.mozz.htmlnative.exception.AttrApplyException;
import com.mozz.htmlnative.view.LayoutParamsCreator;
import ohos.agp.components.Component;
import ohos.app.Context;

/**
 * @author Yang Tao, 17/3/3.
 */

public abstract class LayoutStyleHandler extends StyleHandler {
    public abstract void applyToChild(Context context, Component v, DomElement domElement, Component
            parent, LayoutParamsCreator paramsCreator, String params, Object value) throws
            AttrApplyException;

    public void setDefaultToChild(Context context, Component v, DomElement domElement, Component parent,
                                  LayoutParamsCreator paramsCreator) throws AttrApplyException {

    }
}
