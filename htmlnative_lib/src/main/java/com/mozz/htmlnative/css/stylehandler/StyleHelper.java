package com.mozz.htmlnative.css.stylehandler;

import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.text.Font;
import ohos.agp.text.RichText;
import ohos.agp.text.RichTextBuilder;
import ohos.agp.text.TextForm;

/**
 * @author Yang Tao, 17/3/22.
 */

public final class StyleHelper {

    private StyleHelper() {
    }

    public static void setPadding(Component view, int padding) {
        if (view != null) {
            view.setPadding(padding, padding, padding, padding);
        }
    }

    public static void setPadding(Component view, int topBottomPadding, int leftRightPadding) {
        if (view != null) {
            view.setPadding(leftRightPadding, topBottomPadding, leftRightPadding, topBottomPadding);
        }
    }

    public static void setPadding(Component view, int top, int left, int bottom, int right) {
        if (view != null) {
            view.setPadding(left, top, right, bottom);
        }
    }

    public static void setTopPadding(Component v, int top) {
        if (v != null) {
            v.setPadding(v.getPaddingLeft(), top, v.getPaddingRight(), v.getPaddingBottom());
        }
    }

    public static void setLeftPadding(Component v, int left) {
        if (v != null) {
            v.setPadding(left, v.getPaddingTop(), v.getPaddingRight(), v.getPaddingBottom());
        }
    }

    public static void setRightPadding(Component v, int right) {
        if (v != null) {
            v.setPadding(v.getPaddingLeft(), v.getPaddingTop(), right, v.getPaddingBottom());
        }
    }

    public static void setBottomPadding(Component v, int bottom) {
        if (v != null) {
            v.setPadding(v.getPaddingLeft(), v.getPaddingTop(), v.getPaddingRight(), bottom);
        }
    }

    public static void setBold(Text textView) {
        if (textView != null) {
            textView.setFont(Font.DEFAULT_BOLD);
        }
    }

    public static void setUnderLine(Text textView) {
        TextForm form = new TextForm();
        form.setUnderline(true);
        RichTextBuilder builder = new RichTextBuilder(form);
        builder.addText(textView.getText());
        RichText text = builder.build();
        textView.setRichText(text);
    }

}
