package com.mozz.htmlnative.css.stylehandler;

import com.mozz.htmlnative.HtmlTag;
import com.mozz.htmlnative.css.InheritStylesRegistry;
import com.mozz.htmlnative.dom.DomElement;
import com.mozz.htmlnative.exception.AttrApplyException;
import com.mozz.htmlnative.view.HNDivLayout;
import com.mozz.htmlnative.view.LayoutParamsCreator;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;

class DivLayoutStyleHandler extends StyleHandler {
    @Override
        public void apply(Context context, Component v, DomElement domElement, Component parent,
                          LayoutParamsCreator paramsCreator, String params, Object value)
            throws AttrApplyException {

        if (InheritStylesRegistry.isInherit(params)) {
            HNDivLayout div = (HNDivLayout) v;
            div.saveInheritStyles(params, value);
        }
    }

    @Override
    public void setDefault(Context context, Component v, DomElement domElement,
                           LayoutParamsCreator paramsCreator, Component parent) throws
            AttrApplyException {
        paramsCreator.height = ComponentContainer.LayoutConfig.MATCH_CONTENT;

        if (domElement.getType().equals(HtmlTag.SPAN)) {
            paramsCreator.width = ComponentContainer.LayoutConfig.MATCH_CONTENT;
        } else {
            paramsCreator.width = ComponentContainer.LayoutConfig.MATCH_PARENT;
        }
    }

    @Override
    public Object getStyle(Component v, String styleName) {
        return ((HNDivLayout) v).getInheritStyle(styleName);
    }
}
