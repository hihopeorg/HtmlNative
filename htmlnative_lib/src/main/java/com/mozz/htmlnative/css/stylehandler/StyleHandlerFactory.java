package com.mozz.htmlnative.css.stylehandler;

import com.google.harmony.flexbox.FlexboxLayout;
import com.mozz.htmlnative.view.HNDivLayout;
import ohos.agp.components.*;
import ohos.agp.components.webengine.WebView;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Yang Tao, 17/4/17.
 */

public final class StyleHandlerFactory {

    private static TextViewStyleHandler sText = new TextViewStyleHandler();
    private static ImageViewStyleHandler sImage = new ImageViewStyleHandler();
    private static DivLayoutStyleHandler sDiv = new DivLayoutStyleHandler();
    private static FlexBoxLayoutStyleHandler sFlex = new FlexBoxLayoutStyleHandler();
    private static WebViewStyleHandler sWebview = new WebViewStyleHandler();
    
    private static Map<Class<? extends Component>, StyleHandler> sAttrHandlerCache = new HashMap<>();
    private static Map<Class<? extends Component>, StyleHandler> sExtraAttrHandlerCache = new
            HashMap<>();

    private StyleHandlerFactory() {

    }

    //TODO there is much can be done when dealing with the StyleHandler
    public static StyleHandler byClass( Class<? extends Component> clazz) {

        if (Text.class.isAssignableFrom(clazz)) {
            return sText;
        } else if (Image.class.isAssignableFrom(clazz)) {
            return sImage;
        } else if (clazz.equals(HNDivLayout.class)) {
            return sDiv;
        } else if (clazz.equals(FlexboxLayout.class)) {
            return sFlex;
        } else if (clazz.equals(WebView.class)) {
            return sWebview;
        } else {
            return null;
        }
    }

    
    public static StyleHandler get(Component view) {
        Class<? extends Component> vClazz = view.getClass();
        StyleHandler styleHandler = sAttrHandlerCache.get(vClazz);
        if (styleHandler == null) {
            styleHandler = byClass(vClazz);
            if (styleHandler != null) {
                sAttrHandlerCache.put(vClazz, styleHandler);
            }
        }

        return styleHandler;

    }

    /**
     * Get styleHandler of view's parent
     */
    public static LayoutStyleHandler parentGet(Component view) {
        ComponentParent parent = view.getComponentParent();
        if (parent instanceof ComponentContainer) {
            StyleHandler parentStyleHandler = get((Component) parent);
            LayoutStyleHandler parentLayoutAttr = null;
            if (parentStyleHandler instanceof LayoutStyleHandler) {
                parentLayoutAttr = (LayoutStyleHandler) parentStyleHandler;
            }
            return parentLayoutAttr;
        } else {
            return null;
        }
    }

    /**
     * Get StyleHandler from extra pools
     */
    
    public static StyleHandler extraGet(Component view) {
        return sExtraAttrHandlerCache.get(view.getClass());
    }

    public static StyleHandler registerExtraStyleHandler(Class<? extends Component> viewClass,
                                                         StyleHandler styleHandler) {
        return sExtraAttrHandlerCache.put(viewClass, styleHandler);
    }

    public static void unregisterExtraStyleHandler(Class<? extends Component> viewClass) {
        sExtraAttrHandlerCache.remove(viewClass);
    }

    public static void clearExtraStyleHandler() {
        sExtraAttrHandlerCache.clear();
    }

    public static void clear() {
        sExtraAttrHandlerCache.clear();
        sAttrHandlerCache.clear();
    }
}
