package com.mozz.htmlnative;

import com.mozz.htmlnative.common.ContextProvider;
import com.mozz.htmlnative.css.stylehandler.StyleHandlerFactory;
import com.mozz.htmlnative.dom.HNHead;
import com.mozz.htmlnative.http.HNHttpClient;
import com.mozz.htmlnative.script.ScriptFactory;
import com.mozz.htmlnative.script.ScriptRunner;
import com.mozz.htmlnative.script.lua.LuaRunner;
import com.mozz.htmlnative.utils.ParametersUtils;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilityPackage;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;

/**
 * @author Yang Tao, 17/2/21.
 */

public final class HNativeEngine {

    private static HNConfig sConfig;
    private volatile static boolean sInit = false;

    private HNativeEngine() {
        HNInternalThread.init();
        HNScriptRunnerThread.init();
    }

    
    private static HNativeEngine sInstance = null;

    public static void init(AbilityPackage application, HNConfig config) {

        ContextProvider.install(application);
        initScreenMetrics(application);

        // currently we have only lua runner
        registerRunner(LuaRunner.class);

        if (config == null) {
            throw new IllegalArgumentException("Config can't be null.");
        }
        sConfig = config;
        sConfig.install();

        sInit = true;
    }

    public static void registerRunner(Class<? extends ScriptRunner> runner) {
        ScriptFactory.register(runner);
    }

    
    public static HNativeEngine getInstance() {
        if (!sInit) {
            throw new IllegalStateException("You must call init() first");
        }
        if (sInstance == null) {
            synchronized (HNativeEngine.class) {
                if (sInstance == null) {
                    sInstance = new HNativeEngine();
                }
            }
        }

        return sInstance;
    }

    public void debugParseProcess() {
        HNLog.setDebugLevel(HNLog.LEXER);
        HNLog.setDebugLevel(HNLog.PARSER);
        HNLog.setDebugLevel(HNLog.CSS_PARSER);
    }

    public void debugRenderProcess() {
        HNLog.setDebugLevel(HNLog.RENDER);
        HNLog.setDebugLevel(HNLog.STYLE);
        HNLog.setDebugLevel(HNLog.PROCESS_THREAD);
    }

    private static void initScreenMetrics( Context context) {
        ParametersUtils.init(context);
    }

    public final void loadView(final Context context, final InputStream inputStream, final
    OnHNViewLoaded onHNViewLoaded) {
        HNRenderThread.runRenderTask(new HNRenderThread.RenderTask(context, inputStream,
                onHNViewLoaded));
    }

    public void loadView(Context context, InputStream inputStream, final Ability ability) {
        loadView(context, inputStream, new OnHNViewLoadedWeak<Ability>(ability) {
            @Override
            public void onViewLoaded( Component v) {
                Ability act = mWeakRef.get();
                if (act != null && !act.isTerminating() && v != null) {
                    act.setUIContent((ComponentContainer) v);
                }
            }

            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onHead(HNHead head) {
            }
        });
    }

    public void loadView(Context context, InputStream inputStream, final ComponentContainer viewGroup) {
        loadView(context, inputStream, new OnHNViewLoadedWeak<ComponentContainer>(viewGroup) {
            @Override
            public void onViewLoaded(Component v) {
                ComponentContainer vv = mWeakRef.get();
                vv.addComponent(v);
            }

            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onHead(HNHead head) {

            }
        });
    }

    public static String version() {
        return HNEnvironment.v;
    }

    public static int versionCode() {
        return HNEnvironment.versionCode;
    }

    public static void registerViewType( HNViewType viewType) {
        HNViewTypeManager.registerViewType(viewType);
    }

    public static void unregisterViewType(HNViewType viewType) {
        HNViewTypeManager.unregisterViewType(viewType);
    }

    public static void clearAllViewType() {
        HNViewTypeManager.clearAllViewType();
    }

    public void destroy() {
        HNSegment.clearCache();
        HNInternalThread.quit();
        HNScriptRunnerThread.quit();
        StyleHandlerFactory.clear();
    }


    public static ImageFetcher getImageViewAdapter() {
        return sConfig.getImageViewAdapter();
    }

    public static void registerViewFactory(String ohosViewClassName, HNRenderer.ViewFactory
            viewFactory) {
        HNRenderer.registerViewFactory(ohosViewClassName, viewFactory);
    }

    public static OnHrefClick getHrefLinkHandler() {
        return sConfig.getHrefLinkHandler();
    }


    public static HNHttpClient getHttpClient() {
        return sConfig.getHttpClient();
    }


    public interface OnHNViewLoaded {
        void onViewLoaded(Component v);

        void onError(Exception e);

        void onHead(HNHead head);
    }

    private abstract class OnHNViewLoadedWeak<T> implements OnHNViewLoaded {
        protected WeakReference<T> mWeakRef;

        public OnHNViewLoadedWeak(T tt) {
            this.mWeakRef = new WeakReference<>(tt);
        }
    }


}
