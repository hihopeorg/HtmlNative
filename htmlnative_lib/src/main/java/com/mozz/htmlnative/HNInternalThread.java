package com.mozz.htmlnative;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

/**
 * @author Yang Tao, 17/5/3.
 */

final class HNInternalThread {

    // for running render task
    private static EventRunner mRenderThread = EventRunner.create("HNInternalThread");
    private static EventHandler mRenderHandler;

    static void init() {
//        mRenderThread.run();
        mRenderHandler = new EventHandler(mRenderThread.create());
    }

    public static void run(Runnable r) {
        mRenderHandler.postTask(r);
    }

    static void quit() {
        mRenderThread.stop();
    }
}
