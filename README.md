# HtmlNative

本项目是基于开源项目hsllany/HtmlNative进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/hsllany/HtmlNative ）追踪到原项目版本。

#### 项目介绍

- 项目名称：HtmlNative
- 所属系列：ohos的第三方组件适配移植
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/hsllany/HtmlNative
- 原项目基线版本：v.0.0-alpha ,sha1: 5a88f2f84af68b65114e3899feb33d3bf859a5c6
- 功能介绍：使用HTML / CSS渲染ohos View，使用Lua来控制其逻辑（不是Webview）
- 编程语言：Java
- 外部依赖：无

#### 效果展示

<img src="screenshot/效果展示.gif"/>

#### 安装教程

##### 方案一：

1.下载har包HtmlNative.har。
2.启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3.在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.har'])
	……
}
```
4.在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

##### 方案二：

  1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址:

```
 repositories {
     maven {
         url 'http://106.15.92.248:8081/repository/Releases/' 
     }
 }
```

  2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
 dependencies {
     implementation 'com.mozz.ohos:htmlnative:1.0.0'
 }
```


#### 

#### 使用说明

首页搜索功能使用
```
所输入的链接中使用下方html支持的属性及css支持的属性
```
1.Html支持情况:
```
<a>, <p>, <h1>, <h2>, <h3>, <h4>, <h5>, <h6>, <input>, <img>, <div>, <br>, <iframe>
属性：style, onclick（需要再lua脚本中定义onclick事件), id, class, href(for <a>), src(for <img>)

```
2.css支持情况：
```
  width, height, background, background-color, background-position, background-size, padding, padding-left, padding-right, padding-top, padding-bottom, margin, margin-top, margin-left, margin-right, margin-bottom, visibility

  font-size, color, line-height, font-style, font-weight, text-align, word-spacing, text-overflow, text-transform

  此外，支持从html中获取颜色和drawable，使用@开头即可，例如：

  background: url(@pic);
  color: @colorPrimary;
```
3.例子:
```
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
    <meta name="viewport"
          content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1"/>
    <style>
	html{
		font-size: 62.5%;

	}

	body{
		margin: 0;
		padding: 0;
		color: #3f484f;
	}

	.header{
	    color:#3f484f;
		padding: 2em;
		background: #db3541;
		-hn-background-color-size: 1.2em 100%;

	}

	.article{
	    padding: 3em
	 }

	 p{
	    padding: 5em 0;
	    font-size: 4em;
	    line-height: 120%
	  }

    </style>
    <title>article</title>
</head>
<body>
<div class="header">
    <span id="leheadline_head_icon">头条 | </span>
    <a href="https://www.sina.com.cn/">NewsWebSite</a>
    <h1>滴滴在硅谷建了研发中心做无人驾驶，还从 Uber 挖了人</h1>
    <span id="leheadline_source">来源：新闻网 2017-05-09</span>
</div>

<div class="article">
    <h2>这是滴滴在海外的第一个实验室</h2>
    <h3>周四，滴滴正式在美国加州山景城宣布了滴滴美国研究院的成立，一个由数十位工程师和研究人员组成的团队，已经入驻这里，从大数据安全和无人驾驶两个方向展开研究。</h3>

    <div class="leheadline_img_wrapper">
        <img src="http://rec.letv.com/detail_template/coop/pic.png" alt="新闻说明"/>
        <span class="leheadline_intro">滴滴美国研究所</span>
    </div>

    <img src="http://rec.letv.com/detail_template/coop/pic.png"/>

    <h3>美国研究院由滴滴研究院副院长弓峰敏带领。弓峰敏是去年 9 月加入滴滴，担任滴滴信息安全战略副总裁和滴滴研究院副院长，并开始组建美国研究院。加盟滴滴之前，他是安全公司 Palo Alto
        Networks 的联合创始人。</h3>

    <div class="leheadline_video_wrapper">
        <video src="http://www.w3school.com.cn/example/html5/mov_bbb.mp4" type="video/mp4"
               controls="controls" poster="coop/pic.png" preload="none"></video>
    </div>

    <div class="leheadline_video_wrapper">
        <video src="http://www.w3school.com.cn/example/html5/mov_bbb.mp4" type="video/mp4"
               controls="controls" poster="coop/pic.png" preload="none"></video>
        <span class="leheadline_intro">说明</span>
    </div>

    <h3>这一次，滴滴还从Uber挖来了查理·米勒（Charlie Miller）。米勒被誉为全球顶级安全专家，曾是一名黑客，从事苹果越狱和安全研究，在加入 Uber
        之前，米勒受雇于苹果。2015 年，米勒完成了对一辆吉普切诺基的远程控制实验。2015 年 9 月，他加入Uber担任汽车安全工程师。</h3>

    <h3>在Twitter上，米勒表示他的工作是帮助滴滴研发和使用的自动驾驶系统抵抗来自外部的攻击和威胁。在宣布加入滴滴美国研究院的消息之后，他又发布推文表示实验室正在招人。</h3>
</div>

<div id="leheadline_other">
    <img src="coop/banner.png"/>
</div>
<script src="http://rec.letv.com/detail_template/script-v1.js"></script>
</body>
</html>
```

4.使用方法:
```
InputStream htmlSource = ...;

// 在需要加载view的时候加载
HNativeEngine.getInstance().loadView(mAbility, htmlSource, new
                    HNativeEngine.OnHNViewLoaded() {

    @Override
    public void onViewLoaded(View v) {
        if (mAbility != null && !mAbility.isDestroyed()) {
            mAbility.setContentView(v);
        }
    }

    @Override
    public void onError(Exception e) {

    }

    @Override
    public void onHead(HNHead head) {

    }
});

```

5.HNLua API
```
HNLua API是一套定义在HtmlNative框架下的精简API，通过该API，可以灵活的控制界面、调整样式、跳转url、以及有限地访问部分ohos的基础能力。
```
#### 版本迭代

- v1.0.0

1.目前支持功能如下：

```
- 使用HTML / CSS渲染ohos View，使用Lua来控制其逻辑（不是Webview）,从而实现加载html5页面

```
#### 版权和许可信息

    Copyright 2016 Hsllany. All rights reserved.
    
        Licensed under the Apache License, Version 2.0 (the "License");
        you may not use this file except in compliance with the License.
        You may obtain a copy of the License at
    
            http://www.apache.org/licenses/LICENSE-2.0
    
        Unless required by applicable law or agreed to in writing, software
        distributed under the License is distributed on an "AS IS" BASIS,
        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
        See the License for the specific language governing permissions and
        limitations under the License.

