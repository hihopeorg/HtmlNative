package com.mozz.htmlnativedemo;

import com.mozz.htmlnativedemo.slice.ExampleListAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ExampleListAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ExampleListAbilitySlice.class.getName());
    }

}
