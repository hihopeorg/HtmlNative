/*
 *  Copyright (C) 2021 hihope
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.mozz.htmlnativedemo;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

import java.util.HashMap;

public class ViewHolder {
    private Component cpt;
    private HashMap<Integer, Component> mViews;
    private int mPosition;
    private Component mConvertView;
    private Context mContext;
    private int mLayoutId;

    public ViewHolder(Context context, ComponentContainer parent, int layoutId, int position) {
        this.mContext = context;
        this.mLayoutId = layoutId;
        this.mPosition = position;
        this.mViews = new HashMap<>();
        mConvertView = LayoutScatter.getInstance(mContext).parse(layoutId, null, false);
        mConvertView.setTag(this);
    }

    public static ViewHolder get(Context context, Component convertView, ComponentContainer parent, int layoutId, int position) {
        if (convertView == null) {
            return new ViewHolder(context, parent, layoutId, position);
        } else {
            ViewHolder holder = (ViewHolder) convertView.getTag();
            holder.mPosition = position;
            return holder;
        }
    }

    /**
     * 通过viewId获取控件
     *
     * @param viewId
     * @return
     */
    public <T extends Component> T getView(int viewId) {
        Component view = mViews.get(viewId);
        if (view == null) {
            view = mConvertView.findComponentById(viewId);
            mViews.put(viewId, view);
        }
        return (T) view;
    }

    public Component getConvertView() {
        return mConvertView;
    }
}
