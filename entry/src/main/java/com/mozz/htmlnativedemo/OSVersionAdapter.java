package com.mozz.htmlnativedemo;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;
import ohos.app.Context;

import java.util.List;

public abstract class OSVersionAdapter<T> extends BaseItemProvider {
    protected Context mContext;
    protected List<T> mDatas;
    private int layoutId;

    public OSVersionAdapter(Context context) {
        this.mContext = context;
    }

    public OSVersionAdapter(Context context, List<T> datas) {
        this.mContext = context;
        this.mDatas = datas;
    }

    public OSVersionAdapter(Context context, List<T> datas, int layoutId) {
        this.mContext = context;
        this.mDatas = datas;
        this.layoutId = layoutId;
    }

    public OSVersionAdapter(Context context, int layoutId) {
        this.mContext = context;
        this.layoutId = layoutId;
    }

    @Override
    public int getCount() {
        return mDatas == null ? 0 : mDatas.size();
    }

    @Override
    public T getItem(int position) {
        return mDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer parent) {
        ViewHolder holder = ViewHolder.get(mContext, convertView, parent, layoutId, position);
        convert(holder, position);
        Text txTitle = holder.getView(ResourceTable.Id_title);
        txTitle.setText(String.valueOf(mDatas.get(position)));
        return holder.getConvertView();
    }

    /**
     * 获取值
     *
     * @return
     */
    public List<T> getDatas() {
        return mDatas;
    }

    /**
     * set 数据进来
     *
     * @param datas
     */
    public void setDatas(List<T> datas) {
        mDatas = datas;
    }

    public abstract void convert(ViewHolder holder, int position);
}
