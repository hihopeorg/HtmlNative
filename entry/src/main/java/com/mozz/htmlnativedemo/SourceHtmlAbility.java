package com.mozz.htmlnativedemo;

import com.mozz.htmlnativedemo.slice.SourceHtmlAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class SourceHtmlAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(SourceHtmlAbilitySlice.class.getName());
    }

}
