package com.mozz.htmlnativedemo;

import com.mozz.htmlnativedemo.slice.LayoutExampleAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class LayoutExampleAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(LayoutExampleAbilitySlice.class.getName());
    }
}
