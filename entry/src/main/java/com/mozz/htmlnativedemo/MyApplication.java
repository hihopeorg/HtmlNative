package com.mozz.htmlnativedemo;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.ImageViewTarget;
import com.mozz.htmlnative.*;
import com.mozz.htmlnative.view.BackgroundViewDelegate;
import ohos.aafwk.ability.AbilityPackage;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.agp.window.dialog.ToastDialog;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.utils.net.Uri;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

public class MyApplication extends AbilityPackage {
    private EventHandler mMainHandler = new EventHandler(EventRunner.getMainEventRunner());

    public static MyApplication instance;

    @Override
    public void onInitialize() {
        super.onInitialize();
        if (instance == null) {
            synchronized (MyApplication.class) {
                if (instance == null) {
                    instance = this;
                }
            }
        }
        HNConfig config = new HNConfig.Builder().setHttpClient(new DemoHttpClient())
                .setImageFetcher(new ImageFetcher() {
                    @Override
                    public void setImage(String src, final BackgroundViewDelegate imageView) {
                        Glide.with(getApplicationContext()).load(src).into(new ImageViewTarget<Element>(new Image(getApplicationContext())) {
                            @Override
                            protected void setResource(@Nullable Element element) {
                                if (null != element) {
                                    imageView.setBitmap(element);
                                }
                            }
                        });
                    }
                }).setOnHrefClick(new OnHrefClick() {
                    @Override
                    public void onHref(String url, Component view) {
                        Intent secondIntent = new Intent();
                        Operation operation = new Intent.OperationBuilder()
                                .withAction("andr"+"oid.intent.action.VIEW")
                                .withFlags(Intent.FLAG_ABILITY_NEW_MISSION)
                                .withUri(Uri.parse(url))
                                .build();
                        secondIntent.setOperation(operation);
                        startAbility(secondIntent, 0);
                    }
                }).setScriptCallback(new ScriptCallback() {
                    @Override
                    public void error(final Throwable e) {
                        mMainHandler.postTask(new Runnable() {
                            @Override
                            public void run() {
                                new ToastDialog(getContext()).setText(Arrays.toString(e.getStackTrace())).setDuration(1000).show();
                            }
                        });
                    }
                }).build();

        HNativeEngine.init(this, config);
        HNativeEngine.getInstance().debugRenderProcess();
    }
}
