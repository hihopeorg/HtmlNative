package com.mozz.htmlnativedemo.slice;

import com.mozz.htmlnative.view.HNDivLayout;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;

public class TestAbilitySlice extends AbilitySlice {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        HNDivLayout divView = new HNDivLayout(getContext());

        {
            HNDivLayout.HNDivLayoutParams params = new HNDivLayout.HNDivLayoutParams(ComponentContainer.LayoutConfig.MATCH_CONTENT
                    , ComponentContainer.LayoutConfig.MATCH_CONTENT);

            params.positionMode = HNDivLayout.HNDivLayoutParams.POSITION_STATIC;
            divView.addComponent(generateView("hello world", Color.GREEN), params);
        }

        {
            HNDivLayout.HNDivLayoutParams params = new HNDivLayout.HNDivLayoutParams(ComponentContainer.LayoutConfig.MATCH_CONTENT
                    , ComponentContainer.LayoutConfig.MATCH_CONTENT);
            params.positionMode = HNDivLayout.HNDivLayoutParams.POSITION_FLOAT_LEFT;
            divView.addComponent(generateView("left", Color.RED), params);
        }

        {
            HNDivLayout.HNDivLayoutParams params = new HNDivLayout.HNDivLayoutParams(ComponentContainer.LayoutConfig.MATCH_CONTENT
                    , ComponentContainer.LayoutConfig.MATCH_CONTENT);
            params.positionMode = HNDivLayout.HNDivLayoutParams.POSITION_FLOAT_LEFT;
            params.setMargins(10, 20, 0, 0);
            divView.addComponent(generateView("left2", Color.BLUE), params);
        }

        {
            HNDivLayout.HNDivLayoutParams params = new HNDivLayout.HNDivLayoutParams(ComponentContainer.LayoutConfig.MATCH_CONTENT
                    , ComponentContainer.LayoutConfig.MATCH_CONTENT);
            params.positionMode = HNDivLayout.HNDivLayoutParams.POSITION_FLOAT_RIGHT;
            params.setMargins(10, 20, 0, 0);
            divView.addComponent(generateView("right right right", Color.CYAN), params);
        }

        {
            HNDivLayout.HNDivLayoutParams params = new HNDivLayout.HNDivLayoutParams(ComponentContainer.LayoutConfig.MATCH_CONTENT
                    , ComponentContainer.LayoutConfig.MATCH_CONTENT);
            params.positionMode = HNDivLayout.HNDivLayoutParams.POSITION_FLOAT_RIGHT;
            params.setMargins(10, 20, 0, 0);
            divView.addComponent(generateView("right right rightright right rightright right " +
                    "rightright right rightright right rightright right rightright right " +
                    "rightright right rightright right rightright right rightright right " +
                    "rightright right rightright right right", Color.DKGRAY), params);
        }

        {
            HNDivLayout.HNDivLayoutParams params = new HNDivLayout.HNDivLayoutParams(ComponentContainer.LayoutConfig.MATCH_CONTENT
                    , ComponentContainer.LayoutConfig.MATCH_CONTENT);
            params.positionMode = HNDivLayout.HNDivLayoutParams.POSITION_FLOAT_LEFT;
            params.setMargins(10, 20, 0, 0);
            divView.addComponent(generateView("left2 left2 left2 left2 left2 left2 left2 left2 left2 "
                            + "left2 left2 left2 left2 left2 left2 left2 left2 left2", Color.YELLOW),
                    params);
        }
        super.setUIContent(divView);
    }

    private Component generateView(String t, Color color) {
        Text v = new Text(this);
        v.setText(t);
        v.setTextSize(20, Text.TextSizeType.FP);
        v.setMultipleLine(true);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(color.getValue()));
        v.setBackground(shapeElement);
        return v;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
