package com.mozz.htmlnativedemo.slice;

import com.mozz.htmlnativedemo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.webengine.WebConfig;
import ohos.agp.components.webengine.WebView;

public class WebViewAbilitySlice extends AbilitySlice {

   public static final String EXTAL_URL = "webview_url";
    static final String IS_LOCAL = "is_local";
    private boolean isLocal = true;//是否为本地链接

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_web_view);
        WebView mWebView = (WebView) findComponentById(ResourceTable.Id_webView);
        WebConfig webConfig = mWebView.getWebConfig();

        String url = intent.getStringParam(EXTAL_URL);
        isLocal = intent.getBooleanParam(IS_LOCAL, false);
        if (isLocal) {
            String URL_LOCAL = "dataability://com.mozz.htmlnativedemo.DataAbility/resources/rawfile/" + url;
// 配置是否支持访问DataAbility资源，默认为true
            webConfig.setDataAbilityPermit(true);
            mWebView.load(URL_LOCAL);
        } else {
            mWebView.load(url);
        }

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
