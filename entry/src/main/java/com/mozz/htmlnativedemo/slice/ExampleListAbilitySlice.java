package com.mozz.htmlnativedemo.slice;

import com.mozz.htmlnativedemo.OSVersionAdapter;
import com.mozz.htmlnativedemo.ResourceTable;
import com.mozz.htmlnativedemo.ViewHolder;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.global.resource.Entry;
import ohos.global.resource.RawFileEntry;

import java.io.IOException;
import java.util.ArrayList;

public class ExampleListAbilitySlice extends AbilitySlice {

    private ListContainer mList;
    private ArrayList<String> arrayList;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_example_list);
        mList = (ListContainer) findComponentById(ResourceTable.Id_list);
        initList();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    private void initList() {
        RawFileEntry fileEntry = getResourceManager().getRawFileEntry("resources/rawfile");
        arrayList = new ArrayList<>();
        try {
            Entry[] entries = fileEntry.getEntries();
            for (int i = 0; i < entries.length; i++) {
                arrayList.add(entries[i].getPath());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        OSVersionAdapter osVersionAdapter = new OSVersionAdapter(getContext(), arrayList, ResourceTable.Layout_layout_item) {
            @Override
            public void convert(ViewHolder holder, int position) {

            }
        };
        mList.setItemProvider(osVersionAdapter);
        mList.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(getBundleName())
                        .withAbilityName("com.mozz.htmlnativedemo.LayoutExampleAbility")
                        .build();
                intent.setParam(LayoutExampleAbilitySlice.EXTRA_KEY_RV_FILE, arrayList.get(i));
                intent.setOperation(operation);
                // 通过AbilitySlice的startAbility接口实现启动另一个页面
                startAbility(intent);
            }
        });
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
