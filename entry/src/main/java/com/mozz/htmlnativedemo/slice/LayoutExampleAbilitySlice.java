package com.mozz.htmlnativedemo.slice;

import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.AttachPopupView;
import com.lxj.xpopup.util.XPopupUtils;
import com.mozz.htmlnative.HNativeEngine;
import com.mozz.htmlnative.dom.HNHead;
import com.mozz.htmlnative.utils.IOUtils;
import com.mozz.htmlnativedemo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;

import java.io.IOException;

public class LayoutExampleAbilitySlice extends AbilitySlice implements Component.ClickedListener {

    private Text mTv_pop;
    private Text mTv_title;
    private CustomAttachPopup2 popup2;
    private DependentLayout mContainer;
    static final String EXTRA_KEY_RV_FILE = "rv_asset_file";
    private String mFileName;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_layout_example);
        mTv_pop = (Text) findComponentById(ResourceTable.Id_tv_pop);
        mTv_title = (Text) findComponentById(ResourceTable.Id_tv_title);
        mContainer = (DependentLayout) findComponentById(ResourceTable.Id_relative_view);
        mTv_pop.setClickedListener(this);

        mFileName = intent.getStringParam(EXTRA_KEY_RV_FILE);
        mTv_title.setText(mFileName);
        load(mFileName);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onClick(Component component) {
        popup2 = new CustomAttachPopup2(getContext());
        new XPopup.Builder(getContext())
                .isDestroyOnDismiss(true) // 对于只使用一次的弹窗，推荐设置这个
                .atView(component)
                .hasShadowBg(false) // 去掉半透明背景
                .asCustom(popup2).show();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    public class CustomAttachPopup2 extends AttachPopupView {
        public CustomAttachPopup2(Context context) {
            super(context, null);
        }

        @Override
        protected int getImplLayoutId() {
            return ResourceTable.Layout_custom_attach_popup2;
        }

        @Override
        protected void onCreate() {
            super.onCreate();
            final Text tv1 = (Text) findComponentById(ResourceTable.Id_tv1);
            final Text tv2 = (Text) findComponentById(ResourceTable.Id_tv2);
            final Text tv3 = (Text) findComponentById(ResourceTable.Id_tv3);
            tv1.setText("Show Html");
            tv2.setText("in Browser");
            tv3.setVisibility(HIDE);
            tv1.setClickedListener(new ClickedListener() {
                @Override
                public void onClick(Component component) {
                    showCode();
                    popup2.dismiss();
                }
            });
            tv2.setClickedListener(new ClickedListener() {
                @Override
                public void onClick(Component component) {
                    showWebview();
                    popup2.dismiss();
                }
            });
            getPopupImplView().setBackground(XPopupUtils.createDrawable(0xFFFFFFFF, popupInfo.borderRadius, 0, popupInfo.borderRadius, 0));
        }

        // 设置状态栏的高度，用以修正自定义位置弹窗的高度
        @Override
        protected int setStatusBarHeight() {
            return 130;
        }

    }

    public void load(final String fileName) {
        try {
        Resource resource = null;
        RawFileEntry fileEntry = getResourceManager().getRawFileEntry("resources/rawfile/" +fileName);
        try {
            resource = fileEntry.openRawFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        HNativeEngine.getInstance().loadView(getContext(), resource, new
                HNativeEngine.OnHNViewLoaded() {

                    @Override
                    public void onViewLoaded(Component v) {
                        mContainer.addComponent(v, new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                                ComponentContainer.LayoutConfig.MATCH_PARENT));
                    }

                    @Override
                    public void onError(Exception e) {

                    }

                    @Override
                    public void onHead(HNHead head) {

                    }
                });
        } catch (Exception e) {
            new ToastDialog(this).setText("load file failed").setDuration(1000).show();
        }
    }

    private void showCode() {
        String code;
        try {
            code = readCode(mFileName);
            Intent secondIntent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName("com.mozz.htmlnativedemo.SourceHtmlAbility")
                    .build();
            secondIntent.setOperation(operation);
            secondIntent.setParam(SourceHtmlAbilitySlice.INTENT_SOURCE_CODE, code);
            startAbility(secondIntent);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    String readCode(String fileName) throws IOException {
        Resource s = null;
        RawFileEntry fileEntry = getResourceManager().getRawFileEntry("resources/rawfile/" + fileName);
        try {
            s = fileEntry.openRawFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] buffer = new byte[s.available()];
        s.read(buffer);
        IOUtils.closeQuietly(s);

        return new String(buffer,"UTF-8");
    }

    private void showWebview() {
            Intent secondIntent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName("com.mozz.htmlnativedemo.WebViewAbility")
                    .build();
            secondIntent.setOperation(operation);
            secondIntent.setParam(WebViewAbilitySlice.EXTAL_URL, mFileName);
            secondIntent.setParam(WebViewAbilitySlice.IS_LOCAL, true);
            startAbility(secondIntent);
    }
}
