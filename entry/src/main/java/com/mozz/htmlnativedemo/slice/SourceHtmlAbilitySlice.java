package com.mozz.htmlnativedemo.slice;

import com.mozz.htmlnativedemo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;

public class SourceHtmlAbilitySlice extends AbilitySlice {
    public static final String INTENT_SOURCE_CODE = "source";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_source_html);
        Text sourceCodeTxt = (Text) findComponentById(ResourceTable.Id_text_html);
        String source = intent.getStringParam(INTENT_SOURCE_CODE);
        if (source != null) {
            sourceCodeTxt.setText(source);
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
