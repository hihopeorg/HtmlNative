package com.mozz.htmlnativedemo.slice;

import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.AttachPopupView;
import com.lxj.xpopup.util.XPopupUtils;
import com.mozz.htmlnative.HNativeEngine;
import com.mozz.htmlnative.dom.HNHead;
import com.mozz.htmlnative.utils.URLUtil;
import com.mozz.htmlnativedemo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.*;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {

    private Text mTv_pop;
    private TextField mSearch;
    private Button mGo;
    RemoteViewLoader mLoader;
    private DependentLayout mContainer;
    static final String EXTAL_URL = "webview_url";
    private CustomAttachPopup2 popup2;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        mTv_pop = (Text) findComponentById(ResourceTable.Id_tv_pop);
        mSearch = (TextField) findComponentById(ResourceTable.Id_search_editbox);
        mGo = (Button) findComponentById(ResourceTable.Id_search_go_btn);
        mContainer = (DependentLayout) findComponentById(ResourceTable.Id_relative_view);
        mTv_pop.setClickedListener(this);
        mGo.setClickedListener(this);
        mLoader = new RemoteViewLoader(this);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_tv_pop:
                popup2 = new CustomAttachPopup2(getContext());
                new XPopup.Builder(getContext())
                        .isDestroyOnDismiss(false) // 对于只使用一次的弹窗，推荐设置这个
                        .atView(component)
                        .hasShadowBg(true) // 去掉半透明背景
                        .asCustom(popup2).show();
                break;
            case ResourceTable.Id_search_go_btn:
                String url = mSearch.getText().toString();

                if (URLUtil.isValidUrl(url)) {
                    mLoader.load(url, new HNativeEngine.OnHNViewLoaded() {
                        @Override
                        public void onViewLoaded(Component v) {
                            mContainer.removeAllComponents();
                            mContainer.addComponent(v, new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                                    ComponentContainer.LayoutConfig.MATCH_PARENT));
                        }

                        @Override
                        public void onError(Exception e) {
                            new ToastDialog(getContext()).setText(e.getMessage()).setDuration(1000).show();
                        }

                        @Override
                        public void onHead(HNHead head) {

                        }
                    });
                } else {
                    new ToastDialog(getContext()).setText("not valid url").setDuration(1000).show();
                }
                break;
        }

    }

    public class CustomAttachPopup2 extends AttachPopupView {
        public CustomAttachPopup2(Context context) {
            super(context, null);
        }

        @Override
        protected int getImplLayoutId() {
            return ResourceTable.Layout_custom_attach_popup2;
        }

        @Override
        protected void onCreate() {
            super.onCreate();
            final Text tv1 = (Text) findComponentById(ResourceTable.Id_tv1);
            final Text tv2 = (Text) findComponentById(ResourceTable.Id_tv2);
            final Text tv3 = (Text) findComponentById(ResourceTable.Id_tv3);
            tv1.setText("Example");
            tv2.setText("in Browser");
            tv3.setText("About");
            tv1.setClickedListener(new ClickedListener() {
                @Override
                public void onClick(Component component) {
                    Intent secondIntent = new Intent();
                    Operation operation = new Intent.OperationBuilder()
                            .withDeviceId("")
                            .withBundleName(getBundleName())
                            .withAbilityName("com.mozz.htmlnativedemo.ExampleListAbility")
                            .build();
                    secondIntent.setOperation(operation);
                    startAbility(secondIntent);
                    popup2.dismiss();
                }
            });
            tv2.setClickedListener(new ClickedListener() {
                @Override
                public void onClick(Component component) {
                    String url = mSearch.getText().toString();
                    if (URLUtil.isValidUrl(url)) {
                        Intent secondIntent = new Intent();
                        Operation operation = new Intent.OperationBuilder()
                                .withDeviceId("")
                                .withBundleName(getBundleName())
                                .withAbilityName("com.mozz.htmlnativedemo.WebViewAbility")
                                .build();
                        secondIntent.setOperation(operation);
                        secondIntent.setParam(EXTAL_URL, url);
                        startAbility(secondIntent);
                    } else {
                        new ToastDialog(getContext()).setText("not valid url").setDuration(1000).show();
                    }
                    popup2.dismiss();
                }
            });
            tv3.setClickedListener(new ClickedListener() {
                @Override
                public void onClick(Component component) {
                    Intent secondIntent = new Intent();
                    Operation operation = new Intent.OperationBuilder()
                            .withDeviceId("")
                            .withBundleName(getBundleName())
                            .withAbilityName("com.mozz.htmlnativedemo.TestAbility")
                            .build();
                    secondIntent.setOperation(operation);
                    startAbility(secondIntent);
                    popup2.dismiss();
                }
            });
            getPopupImplView().setBackground(XPopupUtils.createDrawable(0xFFFFFFFF, popupInfo.borderRadius, 0, popupInfo.borderRadius, 0));
        }

        // 设置状态栏的高度，用以修正自定义位置弹窗的高度
        @Override
        protected int setStatusBarHeight() {
            return 130;
        }

    }

}
