package com.mozz.htmlnativedemo;

import com.mozz.htmlnative.http.HNHttpClient;
import com.mozz.htmlnative.http.HttpRequest;
import com.mozz.htmlnative.http.HttpResponse;
import com.mozz.htmlnative.http.RequestCallback;
import ohos.app.Context;
import okhttp3.*;

import java.io.IOException;
import java.lang.ref.WeakReference;

/**
 * @author Yang Tao, 17/5/30.
 */

public class DemoHttpClient implements HNHttpClient {

    @Override
    public void send(final HttpRequest request, final RequestCallback callback) {

        Request mRequest = new Request.Builder().url(request.getUrl()).build();
        sOkHttpClient.newCall(mRequest).enqueue(new HttpCallback(mContext) {
            @Override
            public void onFailure(Call call, IOException e) {
                HttpResponse.Builder builder = new HttpResponse.Builder();
                builder.setError(e);
                if (callback != null) {
                    callback.onResponse(builder.build());
                }
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                HttpResponse.Builder builder = new HttpResponse.Builder();
                builder.setStatusCode(200);
                builder.setBody(response.body().bytes());
                if (callback != null) {
                    callback.onResponse(builder.build());
                }
            }
        });
    }


    private static OkHttpClient sOkHttpClient = new OkHttpClient();
    private Context mContext;

    private abstract static class HttpCallback implements Callback {

        protected WeakReference<Context> mContextWekRef;

        public HttpCallback(Context context) {
            mContextWekRef = new WeakReference<Context>(context);
        }
    }

}
