package com.mozz.htmlnativedemo;

import com.mozz.htmlnativedemo.slice.WebViewAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.global.resource.RawFileDescriptor;
import ohos.utils.net.Uri;

import java.io.FileNotFoundException;
import java.io.IOException;

public class WebViewAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(WebViewAbilitySlice.class.getName());
    }

}
