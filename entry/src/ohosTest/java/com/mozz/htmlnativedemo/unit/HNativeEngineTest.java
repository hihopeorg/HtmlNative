package com.mozz.htmlnativedemo.unit;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.ImageViewTarget;
import com.mozz.htmlnative.*;
import com.mozz.htmlnative.css.Background;
import com.mozz.htmlnative.dom.HNHead;
import com.mozz.htmlnative.view.BackgroundViewDelegate;
import com.mozz.htmlnative.view.HNDivLayout;
import com.mozz.htmlnativedemo.DemoHttpClient;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class HNativeEngineTest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    private HNConfig.Builder config;

    @Before
    public void setUp() {
        config = new HNConfig.Builder();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void setHttpClient() {
        DemoHttpClient demoHttpClient = new DemoHttpClient();
        config.setHttpClient(demoHttpClient);
        Assert.assertEquals("It's right", config.build().getHttpClient(), demoHttpClient);
    }

    @Test
    public void setImageFetcher() {
        ImageFetcher imageFetcher = new ImageFetcher() {
            @Override
            public void setImage(String src, BackgroundViewDelegate imageView) {

            }
        };
        config.setImageFetcher(imageFetcher);
        Assert.assertEquals("It's right", config.build().getImageViewAdapter(), imageFetcher);
    }

    @Test
    public void setOnHrefClick() {
        OnHrefClick onHrefClick = new OnHrefClick() {
            @Override
            public void onHref(String url, Component view) {

            }
        };
        config.setOnHrefClick(onHrefClick);
        Assert.assertEquals("It's right", config.build().getHrefLinkHandler(), onHrefClick);
    }

    @Test
    public void setScriptCallback() {
        Throwable throwable = new Throwable();
        ScriptCallback scriptCallback = new ScriptCallback() {
            @Override
            public void error(Throwable e) {
                Assert.assertEquals("It's right", e, throwable);
            }
        };
        config.setScriptCallback(scriptCallback);
        scriptCallback.error(throwable);
    }

    @Test
    public void debugRenderProcess() {
        HNativeEngine.getInstance().debugRenderProcess();
        HNLog.d(1, "debugRenderProcess");
    }

    @Test
    public void loadView() {
        try {
            Resource resource = null;
            RawFileEntry fileEntry = sAbilityDelegator.getAppContext().getResourceManager().getRawFileEntry("resources/rawfile/article.html");
            try {
                resource = fileEntry.openRawFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            HNativeEngine.getInstance().loadView(sAbilityDelegator.getAppContext(), resource, new
                    HNativeEngine.OnHNViewLoaded() {

                        @Override
                        public void onViewLoaded(Component v) {
                            Assert.assertNotNull("It's right", v);
                        }

                        @Override
                        public void onError(Exception e) {
                        }

                        @Override
                        public void onHead(HNHead head) {
                        }
                    });
        } catch (Exception e) {
            new ToastDialog(sAbilityDelegator.getAppContext()).setText("load file failed").setDuration(1000).show();
        }
    }

    @Test
    public void setBitmap() {
        Glide.with(sAbilityDelegator.getAppContext()).load("https://n.sinaimg.cn/news/crawl/20170302/18ey-fycaahm6004808.jpg").into(new ImageViewTarget<Element>(new Image(sAbilityDelegator.getAppContext())) {
            @Override
            protected void setResource(Element element) {
                BackgroundViewDelegate image = new BackgroundViewDelegate(new Image(sAbilityDelegator.getAppContext()), null, null);
                image.setBitmap(element);
                Assert.assertNotNull("It's right", image);
            }
        });
    }

    @Test
    public void setHtmlBackground() {
        HNDivLayout divView = new HNDivLayout(sAbilityDelegator.getAppContext());
        divView.setPadding(10, 20, 30, 0);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromRgbaInt(Color.BLACK.getValue()));
        Background background = new Background();
        divView.setHtmlBackground(shapeElement, background);
        Assert.assertEquals("It's right", divView.getHtmlBackground(), background);
    }
}