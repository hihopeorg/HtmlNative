package com.mozz.htmlnativedemo.view;

import com.mozz.htmlnativedemo.*;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import ohos.agp.components.webengine.WebView;
import org.junit.Assert;
import org.junit.Test;

public class HtmlNativeUi {

    @Test
    public void startUi() {
        Ability ability = EventHelper.startAbility(MainAbility.class, null, null, null, null);
        Text mTv_pop = (Text) ability.findComponentById(ResourceTable.Id_tv_pop);
        EventHelper.triggerClickEvent(ability, mTv_pop);
        Assert.assertNotNull("It's right", mTv_pop);
    }

    @Test
    public void startExamples() {
        Ability ability = EventHelper.startAbility(ExampleListAbility.class, null, null, null, null);
        ListContainer list = (ListContainer) ability.findComponentById(ResourceTable.Id_list);
        EventHelper.triggerClickEvent(ability, list.getComponentAt(0));
        Assert.assertNotNull("It's right", list);
    }

    @Test
    public void startArticleHtml() {
        Ability ability = EventHelper.startAbility(LayoutExampleAbility.class, "rv_asset_file", "article.html", null, null);
        DependentLayout container = (DependentLayout) ability.findComponentById(ResourceTable.Id_relative_view);
        EventHelper.inputSwipe(ability, container, 0, 100, 0, 200, 300);
        Assert.assertNotNull("It's right", container);
    }

    @Test
    public void startBackgroundHtml() {
        Ability ability = EventHelper.startAbility(LayoutExampleAbility.class, "rv_asset_file", "background.html", null, null);
        Text mTv_pop = (Text) ability.findComponentById(ResourceTable.Id_tv_pop);
        Assert.assertNotNull("It's right", mTv_pop);
    }

    @Test
    public void startFlexHtml() {
        Ability ability = EventHelper.startAbility(LayoutExampleAbility.class, "rv_asset_file", "flex.html", null, null);
        DependentLayout container = (DependentLayout) ability.findComponentById(ResourceTable.Id_relative_view);
        EventHelper.inputSwipe(ability, container, 0, 100, 0, 200, 300);
        Assert.assertNotNull("It's right", container);
    }

    @Test
    public void startHttpHtml() {
        Ability ability = EventHelper.startAbility(LayoutExampleAbility.class, "rv_asset_file", "http.html", null, null);
        Text mTv_pop = (Text) ability.findComponentById(ResourceTable.Id_tv_pop);
        Assert.assertNotNull("It's right", mTv_pop);
    }

    @Test
    public void startImgHtml() {
        Ability ability = EventHelper.startAbility(LayoutExampleAbility.class, "rv_asset_file", "img.html", null, null);
        DependentLayout container = (DependentLayout) ability.findComponentById(ResourceTable.Id_relative_view);
        EventHelper.inputSwipe(ability, container, 0, 100, 0, 200, 300);
        Assert.assertNotNull("It's right", container);
    }

    @Test
    public void startInlinestyleHtml(){
        Ability ability = EventHelper.startAbility(LayoutExampleAbility.class, "rv_asset_file", "inlinestyle.html", null, null);
        DependentLayout container = (DependentLayout) ability.findComponentById(ResourceTable.Id_relative_view);
        EventHelper.inputSwipe(ability, container, 0, 100, 0, 200, 300);
        Assert.assertNotNull("It's right", container);
    }

    @Test
    public void startLuatestHtml() {
        Ability ability = EventHelper.startAbility(LayoutExampleAbility.class, "rv_asset_file", "luatest.html", null, null);
        DependentLayout container = (DependentLayout) ability.findComponentById(ResourceTable.Id_relative_view);
        EventHelper.inputSwipe(ability, container, 0, 100, 0, 200, 300);
        Assert.assertNotNull("It's right", container);
    }

    @Test
    public void startSelectorHtml() {
        Ability ability = EventHelper.startAbility(LayoutExampleAbility.class, "rv_asset_file", "selector.html", null, null);
        DependentLayout container = (DependentLayout) ability.findComponentById(ResourceTable.Id_relative_view);
        EventHelper.inputSwipe(ability, container, 0, 100, 0, 200, 300);
        Assert.assertNotNull("It's right", container);
    }

    @Test
    public void startTextHtml() {
        Ability ability = EventHelper.startAbility(LayoutExampleAbility.class, "rv_asset_file", "text.html", null, null);
        DependentLayout container = (DependentLayout) ability.findComponentById(ResourceTable.Id_relative_view);
        EventHelper.inputSwipe(ability, container, 0, 100, 0, 200, 300);
        Assert.assertNotNull("It's right", container);
    }

    @Test
    public void startShowHtml() {
        Ability ability = EventHelper.startAbility(SourceHtmlAbility.class, "source", "html", null, null);
        Text container = (Text) ability.findComponentById(ResourceTable.Id_text_html);
        EventHelper.inputSwipe(ability, container, 0, 100, 0, 200, 300);
        Assert.assertNotNull("It's right", container);
    }

    @Test
    public void startWebviewHtml() {
        Ability ability = EventHelper.startAbility(WebViewAbility.class, "webview_url", "article.html", "is_local", true);
        WebView container = (WebView) ability.findComponentById(ResourceTable.Id_webView);
        EventHelper.inputSwipe(ability, container, 0, 100, 0, 200, 300);
        Assert.assertNotNull("It's right", container);
    }

    @Test
    public void startMainWebviewHtml() {
        Ability ability = EventHelper.startAbility(WebViewAbility.class, "webview_url", "https://www.baidu.com", "is_local", false);
        WebView container = (WebView) ability.findComponentById(ResourceTable.Id_webView);
        EventHelper.inputSwipe(ability, container, 0, 100, 0, 200, 300);
        Assert.assertNotNull("It's right", container);
    }

    @Test
    public void startAbout() {
        Ability ability = EventHelper.startAbility(TestAbility.class, null, null, null, null);
        Assert.assertNotNull("It's right", ability);
    }
}